﻿using System.Collections.Generic;

namespace NaCl.Events
{
	/// <summary>
	///	CEDComponent class containing all the reusable code for event dispatching.
	/// </summary>
	public class CEDComponent : ICEventDispatcher
	{
		#region Private properties
		private Dictionary<string, HashSet<CEventCallbackData>> _ceventSet = new Dictionary<string, HashSet<CEventCallbackData>> ();
		#endregion

		#region API
		/// <summary>
		/// Add an event listener of "type". 
		/// </summary>
		public void AddCEventListener(string type, CEventCallback callback, bool once = false)
		{
			HashSet<CEventCallbackData> callbackSet;
			CEventCallbackData callbackData = new CEventCallbackData (callback, once);

			if (_ceventSet.TryGetValue (type, out callbackSet))
			{
				if (!callbackSet.Contains (callbackData))
				{
					callbackSet.Add (callbackData);
				}
			}
			else
			{
				callbackSet = new HashSet<CEventCallbackData> (new CEventCallbackDataComparer());
				callbackSet.Add (callbackData);

				_ceventSet.Add (type, callbackSet);
			}
		}

		/// <summary>
		/// Remove the whole event listener of "type".
		/// </summary>
		public void RemoveCEventListener(string type)
		{
			if (_ceventSet.ContainsKey (type))
			{
				_ceventSet.Remove (type);
			}
		}

		/// <summary>
		/// Remove the "callback" from the event listener "type". If the event listener is empty afterwards, remove it entirely.
		/// </summary>
		public void RemoveCEventListener(string type, CEventCallback callback)
		{
			HashSet<CEventCallbackData> callbackSet;

			if (_ceventSet.TryGetValue (type, out callbackSet))
			{
				CEventCallbackData callbackData = new CEventCallbackData (callback, false);
				callbackSet.Remove (callbackData);

				if (callbackSet.Count == 0)
				{
					_ceventSet.Remove (type);
				}
			}
		}

		/// <summary>
		/// Dispatch an event. If there is no callbacks associated to the event, nothing happens.
		/// </summary>
		public void DispatchCEvent(CEvent cevent)
		{
			HashSet<CEventCallbackData> callbackSet;

			if (_ceventSet.TryGetValue (cevent.type, out callbackSet))
			{
				List<CEventCallbackData> removable = new List<CEventCallbackData> ();

				foreach(CEventCallbackData data in callbackSet)
				{
					data.callback.Invoke (cevent);

					if (data.calledOnce)
					{
						removable.Add (data);
					}
				}

				for (int i = 0; i < removable.Count; ++i)
				{
					callbackSet.Remove (removable[i]);
				}
			}
		}

		/// <summary>
		/// Return true if an event listener of "type" exists.
		/// </summary>
		public bool HasCEventListener(string type)
		{
			return _ceventSet.ContainsKey(type);
		}
		#endregion
	}
}
