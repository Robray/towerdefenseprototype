﻿namespace NaCl.Events
{
	/* Example :
	 * 
	 * 	public void test1(CEvent cevent)
	 *	{
	 *		Debug.Log ("Test1"+ cevent.type + cevent.dispatcher + (cevent.args as CharacterDefaultCEArgs).variable);
	 *	}
	 * 
	 *  AddCEventListener(CharacterCE.event, test1, false);
	 *  DispatchCEvent(new CEvent(this, CharacterCE.test, new CharacterDefaultCEArgs()));
	 *  HasCEventListener (CharacterCE.event)
	 *  RemoveCEventListener (CharacterCE.event, test1);
	 */

	/// <summary>
	///	ICEventDispatcher interface.
	/// </summary>
	public interface ICEventDispatcher
	{
		void AddCEventListener(string type, CEventCallback callback, bool once = false);
		void RemoveCEventListener(string type);
		void RemoveCEventListener(string type, CEventCallback callback);
		void DispatchCEvent(CEvent cevent);
		bool HasCEventListener(string type);
	}
}