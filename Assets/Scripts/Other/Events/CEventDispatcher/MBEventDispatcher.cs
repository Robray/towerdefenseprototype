﻿using UnityEngine;
using System.Collections.Generic;

namespace NaCl.Events
{
	/// <summary>
	///	MBCEventDispatcher base class inherited from MonoBehaviour.
	/// </summary>
	public class MBEventDispatcher : MonoBehaviour, ICEventDispatcher
	{
		private CEDComponent ced = new CEDComponent();

		public void AddCEventListener(string type, CEventCallback callback, bool once = false)
		{
			ced.AddCEventListener (type, callback, once);
		}
		public void RemoveCEventListener(string type)
		{
			ced.RemoveCEventListener (type);
		}
		public void RemoveCEventListener(string type, CEventCallback callback)
		{
			ced.RemoveCEventListener (type, callback);
		}
		public void DispatchCEvent(CEvent cevent)
		{
			ced.DispatchCEvent (cevent);
		}
		public bool HasCEventListener(string type)
		{
			return ced.HasCEventListener (type);
		}
	}
}