﻿namespace NaCl.Events
{
    public static class IDCE
	{
		/*Example:
		public static class Game
		{
			public const string onGameStart = "game_onGameStart";
		}
		*/

		public static class Game
		{
			public const string onGameLaunch = "game_onGameLaunch";

			public const string onSceneLoaded = "game_onSceneLoaded"; //when a scene is loaded
			public const string onMainMenuSceneLoaded = "game_onMainMenuSceneLoaded"; //when the main menu is loaded
			public const string onLevelSceneLoaded = "game_onLevelSceneLoaded"; //when a level is loaded

			public const string onLevelStart = "game_onLevelStart";
			public const string onLevelWin = "game_onLevelWin";
			public const string onLevelLose = "game_onLevelLose";
			public const string onLevelEnd = "game_onLevelEnd";
		}

		public static class Entity
		{
			public const string onEnemyActivated = "entity_onEnemyActivated";
			public const string onEnemyDeactivated = "entity_onEnemyDeactivated";

			public const string onEnemyReachGoal = "entity_onEnemyReachGoal";
			public const string onEnemyKilled = "entity_onEnemyKilled";

			public const string onTowerActivated = "entity_onTowerActivated";
			public const string onTowerDeactivated = "entity_onTowerDeactivated";

			public const string onTowerBought = "entity_onTowerBought";
			public const string onTowerDestroyed = "entity_onTowerDestroyed";
			public const string onTowerUpgradeShow = "entity_onTowerUpgradeShow";
			public const string onTowerUpgradeHide = "entity_onTowerUpgradeHide";
		}

		public static class Environment
		{
			public const string onSpawnAreaActivated = "environment_onSpawnAreaActivated";
			public const string onSpawnAreaDeactivated = "environment_onSpawnAreaDeactivated";

			public const string onLifeAreaActivated = "environment_onLifeAreaActivated";
			public const string onLifeAreaDeactivated = "environment_onLifeAreaDeactivated";

			public const string onTowerBuildAreaActivated = "environment_onTowerBuildAreaActivated";
			public const string onTowerBuildAreaDeactivated = "environment_onTowerBuildAreaDeactivated";
		}
    }
}