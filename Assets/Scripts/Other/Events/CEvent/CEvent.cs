﻿namespace NaCl.Events
{
	/// <summary>
	///	CEvent class.
	/// </summary>
	public class CEvent 
	{
		#region Public properties
		public string type;
		public ICEventDispatcher dispatcher;
		public CEventArgs args;
		#endregion
		
		#region API
		public CEvent (ICEventDispatcher dispatcher, string type, CEventArgs args)
		{
			this.type = type;
			this.dispatcher = dispatcher;
			this.args = args;
		}	
		#endregion
	}
}