﻿namespace NaCl.Events
{
	public static class IDCEArgs
	{
		/*Example:
		public class CharacterDefault : CEventArgs 
		{
			//Add necessary variables + constructor
		}
		*/

		/// <summary>
		/// Default entity ceventargs class. Only containing a reference to an entity.
		/// </summary>
		public class EntityDefault : CEventArgs 
		{
			public Entity entity;
			public EntityDefault(Entity e)
			{
				this.entity = e;
			}
		}

		/// <summary>
		/// Default area ceventargs class. Only containing a reference to an area.
		/// </summary>
		public class AreaDefault : CEventArgs 
		{
			public Area area;
			public AreaDefault(Area a)
			{
				this.area = a;
			}
		}
	}
}