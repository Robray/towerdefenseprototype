﻿using UnityEngine;
using System.Collections;

public interface ITargetable 
{
	ITargetable GetTarget();
}