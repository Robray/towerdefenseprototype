﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[RequireComponent (typeof(Collider))]
public class ColliderCollector : MonoBehaviour 
{
	public LayerMask collectableLayer = 0;
	public LayerMask potentialLayerChanger = 0; //layer of gameobjects that could potentialy change layer (ex: from layer Player to PlayerNoClip)

	public List<GameObject> TargetList { get { CleanLists (); return _targetList; } }
	public List<GameObject> IgnoredObj { get { return _ignoredObj; } }

	public int MaxCapacity { get{ return _maxCapacity; } set{ _maxCapacity = value; } }

	protected List<GameObject> _ignoredObj = new List<GameObject>();

	protected List<GameObject> _targetList = new List<GameObject>();
	protected List<GameObject> _changerList = new List<GameObject>();
	protected List<GameObject> _pendingList = new List<GameObject>();

	protected int _maxCapacity = -1;
	protected bool _listsHaveBeenCleaned = false;

	void Update()
	{
		CleanLists ();
		UpdatePending ();

		_listsHaveBeenCleaned = false;
	}

	void OnTriggerEnter(Collider coll)
	{
		int collision = (1 << coll.gameObject.layer) & collectableLayer.value;
		if (collision > 0) 
		{
			AddObject (coll.gameObject);
		}
	}

	void OnTriggerExit(Collider coll)
	{
		int collision = (1 << coll.gameObject.layer) & collectableLayer.value;
		if (collision > 0)
		{
			RemoveObject (coll.gameObject);
		}
	}

	void OnDisable() 
	{
		_targetList.Clear();
		_changerList.Clear();
		_pendingList.Clear ();
	}

	protected void AddObject (GameObject go)
	{
		if (IgnoredObj != null) 
		{
			for(int i = 0; i < IgnoredObj.Count; ++i)
				if(IgnoredObj[i] == go)
					return;
		}

		if (IsAtMaxCapacity ()) 
		{
			_pendingList.Add (go);
		}
		else
		{
			_targetList.Add (go);
		}

		int collision = (1 << go.layer) & potentialLayerChanger.value;
		if (collision > 0)
			_changerList.Add (go);
	}

	protected void RemoveObject(GameObject go)
	{
		_targetList.Remove (go);
		_changerList.Remove (go);
		_pendingList.Remove (go);
	}

	protected bool IsAtMaxCapacity()
	{
		if (_maxCapacity >= 0) 
		{
			if (_targetList.Count >= MaxCapacity) 
			{
				return true;
			}
		}

		return false;
	}

	protected void CleanLists()
	{
		if (_listsHaveBeenCleaned)
			return;

		if(_changerList.Count > 0)
		{
			for(int i = 0; i < _changerList.Count; ++i)
			{
				GameObject target = _changerList [i];
				if (target == null) 
				{
					RemoveObject (target); 
					--i;
				}
				else
				{
					int collectable = (1 << target.layer) & collectableLayer.value;
					bool reset = target.activeInHierarchy ? collectable <= 0 : true;

					if(reset)
					{
						RemoveObject (target);
						--i;
					}
				}
			}
		}

		_listsHaveBeenCleaned = true;
	}

	protected void UpdatePending()
	{
		if (_pendingList.Count > 0) 
		{
			while(!IsAtMaxCapacity() && _pendingList.Count > 0)
			{
				GameObject target = _pendingList [0];

				_targetList.Add (target);
				_pendingList.Remove (target);
			}
		}
	}
}