﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// TargetCollector class.
/// </summary>
public class TargetCollector : MonoBehaviour 
{
	public LayerMask collectableLayer = 0;
	public LayerMask potentialLayerChanger = 0; //layer of gameobjects that could potentialy change layer (ex: from layer Player to PlayerNoClip)

	public List<ITargetable> TargetList {get{CleanLists (); return _targetList;}}
	public List<ITargetable> IgnoredTarget { get { return _ignoredTarget; } }

	public int MaxCapacity { get{ return _maxCapacity; } set{ _maxCapacity = value; } }

	private List<ITargetable> _ignoredTarget = new List<ITargetable>();

	private List<ITargetable> _targetList = new List<ITargetable>();
	private List<ITargetable>  _pendingList = new List<ITargetable>();
	private Dictionary<ITargetable, int> _targetSet = new Dictionary<ITargetable, int>();
	private Dictionary<GameObject, ITargetable>  _objectSet = new Dictionary<GameObject, ITargetable>();
	private Dictionary<GameObject, ITargetable>  _changerSet = new Dictionary<GameObject, ITargetable>();

	private int _maxCapacity = -1;
	private bool _listsHaveBeenCleaned = false;

	public delegate void TriggerEvent(ITargetable target);
	public TriggerEvent OnTriggerEnterEvent;
	public TriggerEvent OnTriggerExitEvent;

	void Update()
	{
		CleanLists ();
		UpdatePending ();

		_listsHaveBeenCleaned = false;
	}

	void OnTriggerEnter(Collider coll)
	{
		int collision = (1 << coll.gameObject.layer) & collectableLayer.value;
		if (collision > 0) 
		{
			AddObject (coll.gameObject);
		}
	}

	void OnTriggerExit(Collider coll)
	{
		int collision = (1 << coll.gameObject.layer) & collectableLayer.value;
		if (collision > 0)
		{
			RemoveObject (coll.gameObject);
		}
	}

	void OnDisable() 
	{
		_targetList.Clear();
		_pendingList.Clear ();

		_targetSet.Clear ();
		_objectSet.Clear ();
		_changerSet.Clear();
	}

	private void AddObject (GameObject go)
	{
		ITargetable collidingTarget = go.GetComponent<ITargetable> ();
		ITargetable finalTarget = collidingTarget != null ? collidingTarget.GetTarget () : null;

		if (finalTarget == null)
			return;

		if (_ignoredTarget != null) 
		{
			for (int i = 0; i < _ignoredTarget.Count; ++i)
				if (_ignoredTarget [i] == finalTarget)
					return;
		}

		if (_targetSet.ContainsKey (finalTarget))
			_targetSet [finalTarget] += 1;
		else 
		{
			_targetSet.Add (finalTarget, 1);

			if (IsAtMaxCapacity ()) 
			{
				_pendingList.Add (finalTarget);
			}
			else
			{
				_targetList.Add (finalTarget);
			}
		}

		_objectSet.Add (go, finalTarget);

		int collision = (1 << go.layer) & potentialLayerChanger.value;
		if (collision > 0)
			_changerSet.Add (go, finalTarget);

		if (OnTriggerEnterEvent != null)
			OnTriggerEnterEvent.Invoke (finalTarget);
	}

	private void RemoveObject(GameObject go)
	{
		ITargetable target;
		if (_objectSet.TryGetValue (go, out target)) 
		{
			_objectSet.Remove (go);
			_changerSet.Remove (go);

			int objectNbr;
			if (_targetSet.TryGetValue (target, out objectNbr)) 
			{
				--objectNbr;

				if (objectNbr <= 0) 
				{
					_targetSet.Remove (target);

					_targetList.Remove (target);
					_pendingList.Remove (target);

					if (OnTriggerExitEvent != null)
						OnTriggerExitEvent.Invoke (target);
				} 
				else
				{
					_targetSet [target] = objectNbr;
				}
			}
		}
	}

	private bool IsAtMaxCapacity()
	{
		if (_maxCapacity >= 0) 
		{
			if (_targetList.Count >= MaxCapacity) 
			{
				return true;
			}
		}

		return false;
	}

	private void CleanLists()
	{
		if (_listsHaveBeenCleaned)
			return;

		if(_changerSet.Count > 0)
		{
			IEnumerable<KeyValuePair<GameObject, ITargetable>> changerSetEnum = _changerSet.AsEnumerable();
			for(int i = 0; i < changerSetEnum.Count(); ++i)
			{
				KeyValuePair<GameObject, ITargetable> keyValue = changerSetEnum.ElementAt (i);
				if (keyValue.Key == null) 
				{
					RemoveObject (keyValue.Key);
					--i;
				}
				else
				{
					//bool reset = keyValue.Key.activeInHierarchy ? Physics.GetIgnoreLayerCollision(keyValue.Key.layer, gameObject.layer) : true;
					int collectable = (1 << keyValue.Key.layer) & collectableLayer.value;
					bool reset = keyValue.Key.activeInHierarchy ? collectable <= 0 : true;

					if(reset)
					{
						RemoveObject (keyValue.Key);
						--i;
					}
				}
			}
		}

		_listsHaveBeenCleaned = true;
	}

	private void UpdatePending()
	{
		if (_pendingList.Count > 0) 
		{
			while(!IsAtMaxCapacity() && _pendingList.Count > 0)
			{
				ITargetable target = _pendingList [0];

				_targetList.Add (target);
				_pendingList.Remove (target);
			}
		}
	}
}
