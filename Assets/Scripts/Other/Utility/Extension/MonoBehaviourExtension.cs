﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
///	MonoBehaviourExtension class.
/// </summary>
public static class MonoBehaviourExtension
{
	//Actions

	public static Coroutine DelayedAction(this MonoBehaviour mb, System.Action action, float time)
	{
		return mb.StartCoroutine (DelayedActionProcess (action, time));
	}
	private static IEnumerator DelayedActionProcess(System.Action action, float time)
	{
		yield return new WaitForSeconds (time);
		action.Invoke ();
	}

	//Coroutine

	public static void StopCoroutineSafe(this MonoBehaviour mb, ref Coroutine cr)
	{
		if (cr == null)
			return;

		mb.StopCoroutine (cr);
		cr = null;
	}
	public static IEnumerator CoroutineProgressUnscaled(this MonoBehaviour mb, float duration, Action<float> loopAction, Action endAction, CoroutineProgressData config = null)
	{ return mb.CoroutineProgressUnscaled (duration, 0, loopAction, endAction, config);}
	public static IEnumerator CoroutineProgressUnscaled(this MonoBehaviour mb, float duration, float delay, Action<float> loopAction, Action endAction, CoroutineProgressData config = null)
	{
		if (delay > 0)
			yield return new WaitForSecondsRealtime (delay);

		if (config == null)
			config = new CoroutineProgressData ();

		float startTime = Time.unscaledTime;
		float t = 0;

		while (t < 1) 
		{
			t = Mathf.Clamp01(MathUtils.SafeDivide(Time.unscaledTime - startTime, duration, 1));

			if(loopAction != null)
				loopAction (t);

			yield return new WaitForSecondsRealtime(config.timeToWait);
		}

		if(endAction != null)
			endAction ();
	}

	public static IEnumerator CoroutineProgress(this MonoBehaviour mb, float duration, Action<float> loopAction, Action endAction, CoroutineProgressData config = null)
	{ return mb.CoroutineProgress (duration, 0, loopAction, endAction, config);}
	public static IEnumerator CoroutineProgress(this MonoBehaviour mb, float duration, float delay, Action<float> loopAction, Action endAction, CoroutineProgressData config = null)
	{
		if (delay > 0)
			yield return new WaitForSeconds (delay);

		if (config == null)
			config = new CoroutineProgressData ();

		float startTime = Time.time;

		Func<YieldInstruction> getYieldClass = null;
		switch (config.type) 
		{
		case CoroutineProgressType.WaitForEndOfFrame:
			getYieldClass = () => {return new WaitForEndOfFrame();};
			break;
		case CoroutineProgressType.WaitForFixedUpdate:
			getYieldClass = () => {return new WaitForFixedUpdate();};
			break;
		case CoroutineProgressType.WaitForSeconds:
			getYieldClass = () => {return new WaitForSeconds(config.timeToWait);};
			break;
		default:
			throw new ArgumentOutOfRangeException ();
		}

		float t = 0;

		while (t < 1) 
		{
			t = Mathf.Clamp01(MathUtils.SafeDivide(Time.time - startTime, duration, 1));

			if(loopAction != null)
				loopAction (t);

			yield return getYieldClass ();
		}

		if(endAction != null)
			endAction ();
	}

	public enum CoroutineProgressType
	{
		WaitForEndOfFrame,
		WaitForFixedUpdate,
		WaitForSeconds,
	}

	public class CoroutineProgressData
	{
		public CoroutineProgressType type;
		public float timeToWait;

		public CoroutineProgressData()
		{
			type = CoroutineProgressType.WaitForEndOfFrame;
			timeToWait = 0.01f;
		}

		public CoroutineProgressData(CoroutineProgressType type, float timeToWait)
		{
			this.type = type;
			this.timeToWait = timeToWait;
		}
	}
}