﻿using UnityEngine;
using System.Collections;

/// <summary>
///	MathUtils class.
/// </summary>
public static class MathUtils
{
	/// <summary>
	/// Safe division. "a" divided by "b"
	/// </summary>
	public static float SafeDivide(float a, float b, float customReturn)
	{
		return b == 0 ? customReturn : a / b;
	}

	public static int[] GetRandomIntArray(int count, int min = 0, int max = int.MaxValue)
	{
		int[] result = new int[count];

		for (int i = 0; i < result.Length; ++i) 
		{
			result [i] = Random.Range (min, max);
		}

		return result;
	}
}
