﻿#region Import
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#endregion

namespace NaCl.ObjectPool
{
	/// <summary>
	/// Pool class used to store objects. Objects can be taken from the pool, if all are used, new ones will be 
	/// instantiated. Taken object needs to be put back manually in the pool whenever it is not used anymore.
	/// </summary>
	[System.Serializable] 
	public class MBObjectPool<T> where T : class, IPoolable<T>
	{
		#region Public properties
		public Transform parent = null;
		[Space(5)]
		public T obj = null;
		public int startSize = 0;
		public int addBy = 0;
		#endregion


		#region Private properties
		private List<T> _list = new List<T>();
		private int _usedCount = 0;
		private bool _hasBeenInitialized = false;

		private bool HasBeenInitialisedLog
		{
			get
			{
				#if UNITY_EDITOR
				if(!_hasBeenInitialized)
					Debug.Log("pool has never been initialized");
				#endif

				return _hasBeenInitialized;
			}
		}
		#endregion
		
		
		#region Constructor
		public MBObjectPool()
		{
			this.obj = null;
			this.startSize = 0;
			this.addBy = 0;
			this.parent = null;
		}
		/// <summary>
		/// Pool constructor, needs the "obj" to pool, "start" number of objects, how much objects to "add" if no
		/// available on get call.
		/// </summary>
		public MBObjectPool(T obj, int start, int add)
		{
			this.obj = obj;
			this.startSize = start;
			this.addBy = add;
			this.parent = null;
		}
		/// <summary>
		/// Pool constructor, needs the "obj" to pool, "start" number of objects, how much objects to "add" if no
		/// available on get call, the "parent" transform.
		/// </summary>
		public MBObjectPool(T obj, int start, int add, Transform parent)
		{
			this.obj = obj;
			this.startSize = start;
			this.addBy = add;
			this.parent = parent;
		}
		#endregion
		
		
		#region API
		/// <summary>
		/// Initialize the pool's list based on the "startSize" property.
		/// </summary>
		public void Initialize()
		{
			if(_hasBeenInitialized)
			{
				#if UNITY_EDITOR
				Debug.Log("pool has already been initialized");
				#endif
				return;
			}
			
			ExtendList(startSize, false);
			_hasBeenInitialized = true;
		}

		/// <summary>
		/// Clear the pool's list and destroy all objects
		/// </summary>
		public void Clear()
		{
			if(!HasBeenInitialisedLog)
				return;

			for (int i = 0; i < _list.Count; ++i)
			{
				Component obj = (_list [i] as Component);
				if(obj != null && obj.gameObject != null)
					GameObject.Destroy(obj.gameObject);
			}

			_list.Clear ();
		}

		/// <summary>
		/// Put back all used objects
		/// </summary>
		public void Reset()
		{
			if(!HasBeenInitialisedLog)
				return;

			for (int i = 0; i < _usedCount; ++i)
			{
				//PutBack(_list[i]);
				_list[i].Dispose();
			}
		}

		/// <summary>
		/// Get the first free object from pool. If none are available, new ones will be instantiated base on the
		/// ObjectPool's "addBy" property.
		/// </summary>
		public T TakeObject()
		{
			if(!HasBeenInitialisedLog)
				return null;
			
			T pObj = null;
				
			do
			{
				int listRange = _list.Count;
				bool extensionNeeded = false;
				
				if (listRange <= _usedCount)
					extensionNeeded = true;
				
				if(extensionNeeded)
				{
					if(!ExtendList(addBy))
						return null;
					
					listRange = _list.Count;
				}
				
				pObj = _list[listRange-1];

				if(pObj == null)
					_list.RemoveAt(listRange-1);

			} while(pObj == null);

			_list.Remove(pObj);
			_list.Insert(0, pObj);

			_usedCount++;
			
			return pObj;
		}
		
		/// <summary>
		/// Put back a taken object in the pool. If the object is not from this pool nothing will happen.
		/// </summary>
		public bool PutBack(T obj)
		{
			if(!HasBeenInitialisedLog || obj == null)
				return false;
			
			T pObj = null;
			
			for(int i = 0; i < _usedCount; ++i)
			{
				if(_list[i] == obj)
				{
					pObj = _list[i];
					break;
				}
			}
			
			if(pObj == null)
			{
				#if UNITY_EDITOR
				Debug.Log(obj.ToString()+" not from this pool or has already been reseted");
				#endif
				
				return false;
			}

			_list.Remove (pObj);
			_list.Add (pObj);

			(pObj as Component).transform.SetParent(parent, false);
			(pObj as Component).gameObject.SetActive(false);
			
			_usedCount--;

			return true;
		}
		#endregion

		#region Private methods
		private bool ExtendList(int nbr, bool log = true)
		{
			if(nbr <= 0)
			{
				#if UNITY_EDITOR
				if(log)
					Debug.Log("pool extension impossible : tried to add "+nbr+" objects");
				#endif
				
				return false;
			}
			if (obj == null) 
			{
				#if UNITY_EDITOR
				Debug.Log("pool extension impossible : obj is null");
				#endif
				
				return false;
			}

			bool isComponent = typeof(T).IsSubclassOf (typeof(Component)) || typeof(T) == typeof(Component);

			if(isComponent == false)
			{
				#if UNITY_EDITOR
				Debug.Log("pooling objects other than Component type (or inherited from) is not supported yet");
				#endif

				return false;
			}

			for(int i = 0; i < nbr; ++i)
			{
				T newInstance = GameObject.Instantiate((obj as Component).gameObject).GetComponent<T>();
					
				newInstance.LinkToPool(this);

				(newInstance as Component).transform.SetParent(parent, false);
				(newInstance as Component).gameObject.SetActive(false);

				_list.Add(newInstance);
			}
			
			#if UNITY_EDITOR
			if(log)
				Debug.Log("pool extented of "+nbr+" \""+obj.ToString()+"\" objects");
			#endif
			
			return true;
		}
		#endregion
		
		#region SubClass
		#endregion
	}
}