﻿namespace NaCl.ObjectPool
{
	public interface IPoolable<T> where T : class, IPoolable<T>
	{
		bool Dispose();
		void LinkToPool(MBObjectPool<T> pool);
	}
}