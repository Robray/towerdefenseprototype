﻿#region Import
using UnityEngine;
using System.Collections;
using NaCl.Events;
#endregion

namespace NaCl.ObjectPool
{
	/// <summary>
	/// Helper base class to allow disposing of object through the object itself.
	/// </summary>
	public abstract class MBPoolableObj<T> : MBEventDispatcher, IPoolable<T>  where T : class, IPoolable<T>
	{
		#region Private properties
		private MBObjectPool<T> _linkedPool;
	    #endregion


	    #region API
		public virtual void Reset()
		{
		}

		public virtual bool Dispose()
		{
			StopAllCoroutines();
			CancelInvoke();

			Reset ();

			bool success = false;

			if (_linkedPool != null)
				success = _linkedPool.PutBack (this as T);
			else
				gameObject.SetActive (false);

			return success;
		}

		public void LinkToPool(MBObjectPool<T> pool)
		{
			if(_linkedPool == null)
				_linkedPool = pool;
			else
			{
				#if UNITY_EDITOR
				Debug.Log("object has already been linked to a pool");
				#endif
			}
		}
	    #endregion
	}
}