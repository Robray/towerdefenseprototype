﻿#region Import
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
#endregion


namespace NaCl.ObjectPool
{
	/// <summary>
	/// K = id type, T = object to be pooled, U = T's pool class
	/// </summary>
	[System.Serializable]
	public class PoolByID<K,T,U>  where T : class, IPoolable<T> where U : MBObjectPool<T> 
	{
		public K id;
		public U pool;
	}

	/// <summary>
	/// Composite pool class used to pool objects, inherited from the same class, by id.
	/// K = id type, T = object to be pooled, U = T's pool class, V = PoolById<K,T,U> class
	/// </summary>
	[System.Serializable] 
	public class MBCompositePool<K, T, U, V> where T : class, IPoolable<T> where U : MBObjectPool<T> where V : PoolByID<K,T,U>
	{
		#region Public properties
		public V[] pools;
		#endregion


		#region Private properties
		private Dictionary<K, U> poolDict = new Dictionary<K, U>();
		private bool _hasBeenInitialized = false;

		private bool HasBeenInitialisedLog
		{
			get
			{
				#if UNITY_EDITOR
				if(!_hasBeenInitialized)
					Debug.Log("pools have never been initialized");
				#endif

				return _hasBeenInitialized;
			}
		}
		#endregion
			
		
		#region API
		/// <summary>
		/// Initialize all the pool's list based on the "startSize" property.
		/// </summary>
		public void Initialize()
		{
			if(_hasBeenInitialized)
			{
				#if UNITY_EDITOR
				Debug.Log("pools have already been initialized");
				#endif
				return;
			}

			for(int i = 0; i < pools.Length; ++i)
			{
				pools[i].pool.Initialize();

				if(!poolDict.ContainsKey(pools[i].id))
					poolDict.Add(pools[i].id, pools[i].pool);
				#if UNITY_EDITOR
				else
					Debug.Log("multiple same id \""+pools[i].id+"\" detected");
				#endif
			}

			_hasBeenInitialized = true;
		}

		/// <summary>
		/// Clear all the pool's list and destroy all objects
		/// </summary>
		public void Clear()
		{
			if(!HasBeenInitialisedLog)
				return;

			for(int i = 0; i < pools.Length; ++i)
				pools[i].pool.Clear();
		}

		/// <summary>
		/// Put back all used objects
		/// </summary>
		public void Reset()
		{
			if(!HasBeenInitialisedLog)
				return;

			for (int i = 0; i < pools.Length; ++i)
				pools[i].pool.Reset();
		}

		/// <summary>
		/// Get the first free object from pool based on given "id". If none are available, new ones will be instantiated base on the
		/// ObjectPool's "addBy" property.
		/// </summary>
		public T TakeObject(K id)
		{
			if(!HasBeenInitialisedLog)
				return null;

			T obj = null;
			U pool;

			if(!poolDict.TryGetValue(id, out pool))
			{
				#if UNITY_EDITOR
				Debug.Log("no pool associated to \""+id+"\" id");
				#endif
			}
			else
				obj = pool.TakeObject();

			return obj;
		}
	
		/// <summary>
		/// Put back a taken object in the pool from "id". If the object is not from any pool nothing will happen.
		/// </summary>
		public bool PutBack(T obj, K id)
		{
			if(!HasBeenInitialisedLog)
				return false;

			if(obj == null)
				return false;

			U pool;

			if(!poolDict.TryGetValue(id, out pool))
			{
				#if UNITY_EDITOR
				Debug.Log("no pool associated to "+id+" found");
				#endif

				return false;
			}
			else
				return pool.PutBack(obj);
		}
		#endregion
		
		
		#region Private methods

		#endregion
		
		
		#region SubClass
		#endregion
	}

	#if UNITY_EDITOR

	public class MBCompositePoolProperty : PropertyDrawer
	{
		private float _addPropertyHeight = 40f;

		private int _elementToDelete = 0;

		private float _paddingStartY = 10;
		private float _paddingStartX = 17;

		private float _paddingElements = 5;
		//private float _paddingLines = 5;

		private float _heightLine = 20;

		private float _widthButton = 100;
		private float _widthNumberField = 35;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.PropertyField(position, property, label, true);

			if (property.isExpanded)
			{
				_elementToDelete = EditorGUI.IntField(new Rect(position.xMin + _widthButton + _paddingElements + _paddingStartX, GetYPosition(position, 0), _widthNumberField, _heightLine), _elementToDelete);
				if(GUI.Button(new Rect(position.xMin + _paddingStartX, GetYPosition(position, 0), _widthButton, _heightLine), "Delete element"))
				{
					SerializedProperty prop = property.FindPropertyRelative("pools");
					if(_elementToDelete >= 0 && _elementToDelete < prop.arraySize)
					{
						prop.DeleteArrayElementAtIndex(_elementToDelete);
					}
				}
			}

		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			if (property.isExpanded)
				return EditorGUI.GetPropertyHeight(property) + _addPropertyHeight;
			
			return EditorGUI.GetPropertyHeight(property);
		}

		private float GetYPosition(Rect propRect, int line)
		{
			return propRect.yMax - _addPropertyHeight + _paddingStartY + (line * _heightLine);
		}
	}

	#endif
}