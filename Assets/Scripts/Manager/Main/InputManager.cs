﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	InputManager class.
/// </summary>
public static class InputManager 
{
	#region Public properties
	#endregion
	
	#region Private properties
	#endregion
	
	#region API
	public static int GetTouchCount()
	{
		#if UNITY_EDITOR || UNITY_STANDALONE

		return Input.GetMouseButton(0) ? 1 : 0;

		#else

		return Input.touchCount;

		#endif
	}

	public static Vector3 GetTouchPosition(int index)
	{
		#if UNITY_EDITOR || UNITY_STANDALONE

		return Input.mousePosition;

		#else

		Vector3 position = Vector3.zero;

		if(Input.touchCount > index)
			position = Input.GetTouch(index).position;
		
		return position;

		#endif
	}

	public static Touch GetTouch(int index)
	{
		//TODO: simulate touch for standalone

		return Input.GetTouch (index);
	}
	#endregion
	
	#region Unity

	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
