﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using NaCl.Events;

/// <summary>
///	GameManager class.
/// </summary>
public class AppManager : MBSingleton<AppManager> 
{
	#region Public properties
	public LevelLC LevelController
	{get{return GetCheckController<LevelLC> (ref _levelController);}}

	public MainMenuLC MainMenuController
	{get{return  GetCheckController<MainMenuLC> (ref _mainMenuController);}}
	#endregion
	
	#region Private properties
	private LevelLC _levelController;
	private MainMenuLC _mainMenuController;

	private LocalController _localController;
	#endregion
	
	#region API
	public void ReloadCurrentLevel()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}
	#endregion
	
	#region Unity
	protected override void Awake()
	{
		base.Awake ();

		GlobalEvent.Instance.AddCEventListener(IDCE.Game.onLevelSceneLoaded, OnLevelSceneLoadedCE);
	}

	private void OnDestroy()
	{
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Game.onLevelSceneLoaded, OnLevelSceneLoadedCE);
	}

	private void Start()
	{
		Debug.Log ("AppManager: game launched.");
		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Game.onGameLaunch, null));

		//TODO: temporary, must be replaced by custom scene loader with event handling

		SceneManager.sceneLoaded += (scene, mode) => {
			OnSceneLoaded();
		};

		OnSceneLoaded ();
	}
	#endregion

	#region Private methods
	private void OnLevelSceneLoadedCE(CEvent e)
	{
		_levelController = null;
		_mainMenuController = null;

		_localController = MonoBehaviour.FindObjectOfType<LocalController> ();

		if (_localController == null)
			Debug.Log ("AppManager: no local controller found.");
	}

	private void OnSceneLoaded()
	{
		GlobalEvent.Instance.DispatchCEvent(new CEvent(this, IDCE.Game.onSceneLoaded, null));
		GlobalEvent.Instance.DispatchCEvent(new CEvent(this, IDCE.Game.onLevelSceneLoaded, null));
	}

	private T GetCheckController<T> (ref T controller) where T : LocalController
	{
		if (controller != null)
			return controller;

		controller = _localController as T;	

		if (controller != null)
			return controller;

		controller = new GameObject ("_DEFAULT_CONTROLLER_").AddComponent <T>();

		return controller;
	}
	#endregion
	
	#region SubClass
	#endregion
}
