﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	WaveData class.
/// </summary>
[System.Serializable]
public struct WaveData 
{
	#region Public properties
	public float startDelay; //time before starting the wave by default, negative value = must be started manually
	public SpawnData[] spawn;
	#endregion
	
	#region Private properties
	#endregion
	
	#region API
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	[System.Serializable]
	public struct SpawnData
	{
		public Character mob; //mob to spawn
		public SpawnArea spawnArea; //spawn area location, if empty, location is random between all existing spawn areas
		public LifeArea target; //target location, if empty, location is random between all existing life areas
		public float delay; //spawn delay after wave start
		public int count; //number of mobs
	}
	#endregion
}
