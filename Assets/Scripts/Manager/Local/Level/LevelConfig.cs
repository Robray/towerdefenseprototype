﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using NaCl.Events;

/// <summary>
///	LevelConfig class.
/// </summary>
public class LevelConfig : MBEventDispatcher 
{
	#region Public properties
	public int startLife = 10;
	public int startMoney = 100;
	[Space(5)]
	[ReadOnlyRuntime] public WaveData[] enemyWave;

	public int TotalWaves
	{ get { return enemyWave.Length; } }

	public int CurrentWave
	{ get { return _currentWaveNumber+1; } }

	public int CurrentMoney
	{ get { return _currentMoney; } }

	public int CurrentLife
	{ get { return _currentLife; } }

	public float TimeRemainingBeforeWave
	{ get { return _timeSendQueuedWave < 0 ? _timeSendQueuedWave : Math.Max(0, _timeSendQueuedWave - Time.time); } }

	#endregion
	
	#region Private properties
	private int _currentLife = 0;
	private int _currentMoney = 0;
	private int _currentWaveNumber = 0;
	private int _queuedWaveNumber = -1;

	private bool _levelStarted = false;
	private bool _levelEnded = false;

	private int _totalEnemyCount = 0;
	private int _killedEnemyCount = 0;
	private int _successfulEnemyCount = 0;

	private float _timeSendQueuedWave = -1;

	private Coroutine _waveDelayedCR = null;
	#endregion
	
	#region API
	/// <summary>
	/// Start level. Summon waves of mobs based on level's wave data.
	/// </summary>
	public void StartLevel()
	{
		if (_levelStarted)
			return;

		_levelStarted = true;

		GlobalEvent.Instance.DispatchCEvent(new CEvent(this, IDCE.Game.onLevelStart, null));

		QueueNextWave ();

		Debug.Log ("LevelConfig: level started.");
	}

	/// <summary>
	/// Stop wave summoning.
	/// </summary>
	public void StopLevel()
	{
		if (!_levelStarted)
			return;

		StopAllCoroutines ();

		Debug.Log ("LevelConfig: level stopped.");
	}

	//TODO: ResumeLevel()

	/// <summary>
	/// Reset all level's data.
	/// </summary>
	public void ResetLevel()
	{
		StopAllCoroutines ();

		_currentLife = startLife;
		_currentMoney = startMoney;
		_queuedWaveNumber = -1;
		_currentWaveNumber = 0;

		_killedEnemyCount = 0;
		_successfulEnemyCount = 0;

		_levelStarted = false;
		_levelEnded = false;

		Debug.Log ("LevelConfig: level reset.");
	}

	/// <summary>
	/// Force send the next wave of mobs.
	/// </summary>
	public void SendNextWave()
	{
		if (!_levelStarted || (_queuedWaveNumber < 0 || _queuedWaveNumber >= enemyWave.Length))
			return;

		this.StopCoroutineSafe (ref _waveDelayedCR);

		_currentWaveNumber = _queuedWaveNumber;

		StartCoroutine (WaveSpawnProcess (enemyWave[_queuedWaveNumber]));

		QueueNextWave ();
	}
	#endregion
	
	#region Unity
	private void Awake()
	{
		GlobalEvent.Instance.AddCEventListener(IDCE.Entity.onEnemyReachGoal, OnEnemyReachGoalCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Entity.onEnemyKilled, OnEnemyKilledCE);

		GlobalEvent.Instance.AddCEventListener(IDCE.Entity.onTowerBought, OnTowerBoughtCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Entity.onTowerDestroyed, OnTowerDestroyedCE);

		ResetLevel ();

		SortWaveData ();
		CalculateTotalEnemies ();
	}

	private void OnDestroy()
	{
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Entity.onEnemyReachGoal, OnEnemyReachGoalCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Entity.onEnemyKilled, OnEnemyKilledCE);

		GlobalEvent.Instance.RemoveCEventListener(IDCE.Entity.onTowerBought, OnTowerBoughtCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Entity.onTowerDestroyed, OnTowerDestroyedCE);
	}

	#endregion

	#region Private methods
	/// <summary>
	/// Start countdown before sending the next wave. If wave's delay is negative nothing happens.
	/// </summary>
	private void QueueNextWave()
	{
		if (!_levelStarted)
			return;


		_queuedWaveNumber++;

		if (_queuedWaveNumber >= enemyWave.Length)
		{
			_queuedWaveNumber = enemyWave.Length;
			_timeSendQueuedWave = 0; // No more waves to queue, time should be 0
			return;
		}



		if (enemyWave [_queuedWaveNumber].startDelay >= 0) 
		{
			_waveDelayedCR = StartCoroutine (DelayWaveSpawnProcess (enemyWave [_queuedWaveNumber]));
		}
		else
			_timeSendQueuedWave = -1;  // negative value means, wave must be started manually
	}

	private IEnumerator DelayWaveSpawnProcess(WaveData data)
	{
		_timeSendQueuedWave = Time.time + data.startDelay;

		while (Time.time < _timeSendQueuedWave)
			yield return 0;

		SendNextWave();
	}

	/// <summary>
	/// Start mob spawn based on spawn delay.
	/// </summary>
	private IEnumerator WaveSpawnProcess(WaveData data)
	{
		if (data.spawn.Length == 0)
			yield break;

		float startTime = Time.time;

		int spawnIndex = 0;

		while (spawnIndex < data.spawn.Length) 
		{
			while (Time.time - startTime < data.spawn [spawnIndex].delay)
				yield return 0;

			if (data.spawn [spawnIndex].mob != null) 
			{
				SpawnArea spawnArea = data.spawn [spawnIndex].spawnArea;
				LifeArea target = data.spawn [spawnIndex].target;

				//if null get random
				if (spawnArea == null)
				{
					List<SpawnArea> areaList = AppManager.Instance.LevelController.Game.activeSpawnAreaList;
					spawnArea = areaList.Count == 0 ? null : areaList[UnityEngine.Random.Range(0, areaList.Count)];
				}
				if (target == null)
				{
					List<LifeArea> areaList = AppManager.Instance.LevelController.Game.activeLifeAreaList;
					target = areaList.Count == 0 ? null : areaList[UnityEngine.Random.Range(0, areaList.Count)];
				}


				if (spawnArea == null)
					Debug.Log ("LevelConfig: no area found to spawn mob.");
				else if (target == null)
					Debug.Log ("LevelConfig: no target found.");
				else 
				{
					for (int i = 0; i < data.spawn [spawnIndex].count; ++i) 
					{
						Character character = spawnArea.SpawnEntity (data.spawn [spawnIndex].mob) as Character;
						AppManager.Instance.LevelController.Pool.InstantiateLifeBar (character);

						if(character != null)
							character.GoTo (target.GetRandomPointInsideArea ());
					}
				}
			}

			spawnIndex++;
		}
	}

	/// <summary>
	/// Sort wave spawn data based on delay.
	/// </summary>
	private void SortWaveData()
	{
		for (int i = 0; i < enemyWave.Length; i++) 
		{
			Array.Sort(enemyWave[i].spawn, (a,b) => {
				return a.delay.CompareTo(b.delay);
			});
		}
	}

	/// <summary>
	/// Update total enemies in level.
	/// </summary>
	private void CalculateTotalEnemies()
	{
		_totalEnemyCount = 0;

		for (int i = 0; i < enemyWave.Length; i++) 
		{
			for (int j = 0; j < enemyWave[i].spawn.Length; j++) 
			{
				_totalEnemyCount += Math.Max(0, enemyWave [i].spawn [j].count);
			}
		}
	}

	/// <summary>
	/// Check for end game conditions.
	/// </summary>
	private void CheckEndGame()
	{
		if (_levelEnded)
			return;

		if (_currentLife <= 0) 
		{
			EndGame ();

			GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Game.onLevelLose, null));

			Debug.Log ("LevelConfig: LOSE");
		} 
		else if (_successfulEnemyCount + _killedEnemyCount >= _totalEnemyCount) 
		{
			EndGame ();

			GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Game.onLevelWin, null));

			Debug.Log ("LevelConfig: WIN");
		}
	}

	/// <summary>
	/// End level.
	/// </summary>
	private void EndGame()
	{
		_levelEnded = true;
		StopLevel ();

		GlobalEvent.Instance.DispatchCEvent(new CEvent(this, IDCE.Game.onLevelEnd, null));
	}

	//Events

	//	Enemy

	private void OnEnemyReachGoalCE(CEvent e)
	{
		IDCEArgs.EntityDefault args = e.args as IDCEArgs.EntityDefault;

		if (args == null)
			return;

		Enemy enemy = args.entity as Enemy;

		if (enemy != null)
		{
			_currentLife -= 1;
			_successfulEnemyCount += 1;

			if (_currentLife < 0)
				_currentLife = 0;

			CheckEndGame ();
		}
	}

	private void OnEnemyKilledCE(CEvent e)
	{
		IDCEArgs.EntityDefault args = e.args as IDCEArgs.EntityDefault;

		if (args == null)
			return;

		Enemy enemy = args.entity as Enemy;

		if (enemy != null)
		{
			_killedEnemyCount += 1;
			_currentMoney += enemy.bounty;

			CheckEndGame ();
		}
	}

	//	Tower

	private void OnTowerBoughtCE(CEvent e)
	{
		IDCEArgs.EntityDefault args = e.args as IDCEArgs.EntityDefault;

		if (args == null)
			return;

		Tower tower = args.entity as Tower;

		if (tower != null)
		{
			_currentMoney -= tower.cost;

			if (_currentMoney < tower.cost)
			{
				AppManager.Instance.LevelController.Interaction.DeselectTowerToBuild ();

				if (_currentMoney <= 0) 
					_currentMoney = 0;
			}
		}
	}

	private void OnTowerDestroyedCE(CEvent e)
	{
		IDCEArgs.EntityDefault args = e.args as IDCEArgs.EntityDefault;

		if (args == null)
			return;

		Tower tower = args.entity as Tower;

		if (tower != null)
		{
			_currentMoney += tower.recycleMoney;
		}
	}
	#endregion
	
	#region SubClass
	#endregion
}
