﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.ObjectPool;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
///	PoolCC class.
/// </summary>
[System.Serializable]
public class PoolCC : ControllerComponent 
{
	#region Public properties
	[SerializeField] Data.EntityPoolCoPool _entityPool = new Data.EntityPoolCoPool ();
	[SerializeField] Data.LifeBarPool _lifeBarPool = new Data.LifeBarPool ();

	public bool HasBeenInitialized 
	{get; private set;}

	public Data.EntityPoolCoPool EntityPool
	{ get { return _entityPool; } }

	public Data.LifeBarPool LifeBarPool
	{ get { return _lifeBarPool; } }
	#endregion

	#region Private properties
	#endregion

	#region API
	public override bool Initialize(LocalController link) 
	{
		if (!base.Initialize (link) || HasBeenInitialized)
			return false;
		
		_entityPool.Initialize ();
		_lifeBarPool.Initialize ();

		HasBeenInitialized = true;

		return true;
	}

	public override bool Destroy (LocalController link)  
	{
		if (!base.Destroy (link))
			return false;
		
		_entityPool.Clear ();
		_lifeBarPool.Clear ();

		HasBeenInitialized = false;

		return true;
	}

	public void Reset()
	{
		_entityPool.Reset();
		_lifeBarPool.Reset ();
	}
		
	public T InstantiateEntity<T>(string id, Vector3 position, Quaternion rotation) where T : Entity
	{
		Entity entity = _entityPool.TakeObject(id);

		if (entity != null) 
		{
			entity.gameObject.SetActive (true);
			entity.Position = position;
			entity.transform.rotation = Quaternion.identity;
		}	

		return entity as T;
	}

	public LifeBar InstantiateLifeBar(Character target)
	{
		if (target == null)
			return null;

		LifeBar lifeBar = _lifeBarPool.TakeObject();

		if (lifeBar != null) 
		{
			lifeBar.gameObject.SetActive (true);
			lifeBar.LinkToCharacter (target);
		}	

		return lifeBar;
	}
	#endregion


	#region Unity
	//void Start()
	//{
	//	if(!HasBeenInitialized)
	//		InitializeAll();
	//}
	#endregion


	#region Private methods
	#endregion

	#region SubClass
	public static class Data
	{
		[System.Serializable]
		public class EntityPool : MBObjectPool<Entity>{}
		[System.Serializable]
		public class EntityPoolID : PoolByID<string, Entity, EntityPool>{}
		[System.Serializable]
		public class EntityPoolCoPool : MBCompositePool<string, Entity, EntityPool, EntityPoolID>{}

		[System.Serializable]
		public class LifeBarPool : MBObjectPool<LifeBar>{}


		#if UNITY_EDITOR
		[CustomPropertyDrawer(typeof(EntityPoolCoPool), true)]
		public class EntityPoolProperty : MBCompositePoolProperty{}
		#endif
	}
	#endregion
}
