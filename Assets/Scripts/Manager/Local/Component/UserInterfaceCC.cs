﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaCl.Events;
using System;

/// <summary>
///	UIManager class.
/// </summary>
[System.Serializable]
public class UserInterfaceCC : ControllerComponent
{
	#region Public properties
	#endregion

	#region Private properties
	private UIConfig _activeUI = null;
	#endregion
	
	#region API
	public override bool Initialize(LocalController link) 
	{
		if (!base.Initialize (link))
			return false;

		GlobalEvent.Instance.AddCEventListener(IDCE.Game.onLevelSceneLoaded, OnLevelSceneLoadedCE);

		return true;
	}
	public override bool Destroy (LocalController link)  
	{
		if (!base.Destroy (link))
			return false;

		GlobalEvent.Instance.RemoveCEventListener(IDCE.Game.onLevelSceneLoaded, OnLevelSceneLoadedCE);

		return true;
	}
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	//Events
	//	Main

	private void OnLevelSceneLoadedCE(CEvent e)
	{
		_activeUI = null;
		_activeUI = MonoBehaviour.FindObjectOfType<UIConfig> ();

		if (_activeUI == null)
		{
			Debug.Log ("UserInterfaceCC: no ui config found.");
			AppManager.Instance.LevelController.Interaction.StartCurrentLevel ();
		}
		else
			Debug.Log ("UserInterfaceCC: ui config loaded.");
	}
	#endregion
	
	#region SubClass
	#endregion
}
