﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.Events;

/// <summary>
///	ManagerComponent class.
/// </summary>
[System.Serializable]
public abstract class ControllerComponent : EventDispatcher
{
	#region Public properties
	#endregion

	#region Private properties
	protected LocalController _linkedController = null;
	#endregion
	
	#region API
	/// <summary>
	/// Initialize the component and link the owner.
	/// </summary>
	public virtual bool Initialize(LocalController link) 
	{
		if (_linkedController == null && link != null)
		{
			_linkedController = link;
			return true;
		}
		else
			return false;
	}

	/// <summary>
	/// Destroy the component and if link is owner.
	/// </summary>
	public virtual bool Destroy(LocalController link) 
	{
		if (link != null && _linkedController == link)
			return true;
		else
			return false;
	}
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}