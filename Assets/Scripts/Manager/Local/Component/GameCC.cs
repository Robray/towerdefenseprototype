﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.Events;
using UnityEngine.SceneManagement;

/// <summary>
///	GameController class.
/// </summary>
[System.Serializable]
public class GameCC : ControllerComponent
{
	#region Public properties
	[HideInInspector] public List<LifeArea> activeLifeAreaList = new List<LifeArea>();
	[HideInInspector] public List<SpawnArea> activeSpawnAreaList = new List<SpawnArea>();
	[HideInInspector] public List<TowerArea> activeTowerAreaList = new List<TowerArea>();

	[HideInInspector] public HashSet<Enemy> activeEnemySet = new HashSet<Enemy>();
	[HideInInspector] public HashSet<Tower> activeTowerSet = new HashSet<Tower>();

	public LevelConfig ActiveLevel
	{ get { return _activeLevel; } }
	#endregion
	
	#region Private properties
	private LevelConfig _activeLevel = null;
	#endregion
	
	#region API
	public override bool Initialize(LocalController link) 
	{
		if (!base.Initialize (link))
			return false;

		activeLifeAreaList = new List<LifeArea>();
		activeSpawnAreaList = new List<SpawnArea>();
		activeTowerAreaList = new List<TowerArea> ();

		activeEnemySet = new HashSet<Enemy>();

		GlobalEvent.Instance.AddCEventListener(IDCE.Game.onLevelSceneLoaded, OnLevelSceneLoadedCE);

		GlobalEvent.Instance.AddCEventListener(IDCE.Environment.onSpawnAreaActivated, OnSpawnAreaActivatedCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Environment.onSpawnAreaDeactivated, OnSpawnAreaDeactivatedCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Environment.onLifeAreaActivated, OnLifeAreaActivatedCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Environment.onLifeAreaDeactivated, OnLifeAreaDeactivatedCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Environment.onTowerBuildAreaActivated, OnTowerBuildAreaActivatedCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Environment.onTowerBuildAreaDeactivated, OnTowerBuildAreaDeactivatedCE);

		GlobalEvent.Instance.AddCEventListener(IDCE.Entity.onEnemyActivated, OnEnemyActivatedCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Entity.onEnemyDeactivated, OnEnemyDeactivatedCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Entity.onEnemyReachGoal, OnEnemyReachGoalCE);

		GlobalEvent.Instance.AddCEventListener(IDCE.Entity.onTowerActivated, OnTowerActivatedCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Entity.onTowerDeactivated, OnTowerDeactivatedCE);

		return true;
	}

	public override bool Destroy (LocalController link)  
	{
		if (!base.Destroy (link))
			return false;

		GlobalEvent.Instance.RemoveCEventListener(IDCE.Game.onLevelSceneLoaded, OnLevelSceneLoadedCE);

		GlobalEvent.Instance.RemoveCEventListener(IDCE.Environment.onSpawnAreaActivated, OnSpawnAreaActivatedCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Environment.onSpawnAreaDeactivated, OnSpawnAreaDeactivatedCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Environment.onLifeAreaActivated, OnLifeAreaActivatedCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Environment.onLifeAreaDeactivated, OnLifeAreaDeactivatedCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Environment.onTowerBuildAreaActivated, OnTowerBuildAreaActivatedCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Environment.onTowerBuildAreaDeactivated, OnTowerBuildAreaDeactivatedCE);

		GlobalEvent.Instance.RemoveCEventListener(IDCE.Entity.onEnemyActivated, OnEnemyActivatedCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Entity.onEnemyDeactivated, OnEnemyDeactivatedCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Entity.onEnemyReachGoal, OnEnemyReachGoalCE);

		GlobalEvent.Instance.RemoveCEventListener(IDCE.Entity.onTowerActivated, OnTowerActivatedCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Entity.onTowerDeactivated, OnTowerDeactivatedCE);

		return true;
	}
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	//Events
	//	Main

	private void OnLevelSceneLoadedCE(CEvent e)
	{
		_activeLevel = null;
		_activeLevel = MonoBehaviour.FindObjectOfType<LevelConfig> ();

		if (_activeLevel == null)
			Debug.Log ("GameCC: no level config found.");
		else
			Debug.Log ("GameCC: level config loaded.");
	}

	//	Spawn area

	private void OnSpawnAreaActivatedCE(CEvent e)
	{
		OnAreaActivated<SpawnArea> (e.args, activeSpawnAreaList);
	}
	private void OnSpawnAreaDeactivatedCE(CEvent e)
	{
		OnAreaDeactivated<SpawnArea> (e.args, activeSpawnAreaList);
	}

	//	Life area

	private void OnLifeAreaActivatedCE(CEvent e)
	{
		OnAreaActivated<LifeArea> (e.args, activeLifeAreaList);
	}
	private void OnLifeAreaDeactivatedCE(CEvent e)
	{
		OnAreaDeactivated<LifeArea> (e.args, activeLifeAreaList);
	}

	//	Life area

	private void OnTowerBuildAreaActivatedCE(CEvent e)
	{
		OnAreaActivated<TowerArea> (e.args, activeTowerAreaList);
	}
	private void OnTowerBuildAreaDeactivatedCE(CEvent e)
	{
		OnAreaDeactivated<TowerArea> (e.args, activeTowerAreaList);
	}

	//	Area add remove

	private void OnAreaActivated<T>(CEventArgs args, List<T> addTo) where T : Area
	{
		IDCEArgs.AreaDefault areaArgs = args as IDCEArgs.AreaDefault;

		if (areaArgs == null)
			return;

		T area = areaArgs.area as T;

		if(area != null)
			addTo.Add (area);
	}
	private void OnAreaDeactivated<T>(CEventArgs args, List<T> removeFrom) where T : Area
	{
		IDCEArgs.AreaDefault areaArgs = args as IDCEArgs.AreaDefault;

		if (areaArgs == null)
			return;

		T area = areaArgs.area as T;

		if(area != null)
			removeFrom.Remove (area);
	}

	//	Enemy

	private void OnEnemyReachGoalCE(CEvent e)
	{
		OnEnemyDeactivatedCE (e);
	}
	private void OnEnemyActivatedCE(CEvent e)
	{
		OnEntityActivated<Enemy> (e.args, activeEnemySet);
	}
	private void OnEnemyDeactivatedCE(CEvent e)
	{
		OnEntityDeactivated<Enemy> (e.args, activeEnemySet);
	}

	//	Tower

	private void OnTowerActivatedCE(CEvent e)
	{
		OnEntityActivated<Tower> (e.args, activeTowerSet);
	}
	private void OnTowerDeactivatedCE(CEvent e)
	{
		OnEntityDeactivated<Tower> (e.args, activeTowerSet);
	}

	//	Entity add remove

	private void OnEntityActivated<T>(CEventArgs args, HashSet<T> addTo) where T : Entity
	{
		IDCEArgs.EntityDefault entityArgs = args as IDCEArgs.EntityDefault;

		if (entityArgs == null)
			return;

		T area = entityArgs.entity as T;

		if(area != null)
			addTo.Add (area);
	}
	private void OnEntityDeactivated<T>(CEventArgs args, HashSet<T> removeFrom) where T : Entity
	{
		IDCEArgs.EntityDefault entityArgs = args as IDCEArgs.EntityDefault;

		if (entityArgs == null)
			return;

		T area = entityArgs.entity as T;

		if(area != null)
			removeFrom.Remove (area);
	}
	#endregion
	
	#region SubClass
	#endregion
}
