﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.Events;

/// <summary>
///	InteractionController class.
/// </summary>
public class InteractionCC : ControllerComponent
{
	#region Public properties
	public Tower SelectedTowerToBuild
	{ get { return _selectedTowerToBuild; } }

	public bool FreeTowerAreaDisplayed
	{ get { return _freeTowerAreaDisplayed; } }
	#endregion
	
	#region Private properties
	private Tower _selectedTowerToBuild = null;
	private TowerArea _selectedAreaToUpgrade = null;

	private bool _freeTowerAreaDisplayed = false;
	private bool _builtTowerRangeDisplayed = false;
	#endregion
	
	#region API
	public void StartCurrentLevel()
	{
		LevelConfig activeLevel = AppManager.Instance.LevelController.Game.ActiveLevel;

		if (activeLevel != null)
			activeLevel.StartLevel ();
	}

	/*public void EndCurrentLevel()
	{
		LevelConfig activeLevel = AppManager.Instance.LevelController.Game.ActiveLevel;

		if (activeLevel != null)
			activeLevel.EndLevel ();
	}*/

	public void SendNextWave()
	{
		AppManager.Instance.LevelController.Game.ActiveLevel.SendNextWave ();
		HideTowerUpgrade ();
	}

	public void SelectTowerToBuild(Tower tower)
	{
		DeselectTowerToBuild ();

		if(tower.cost <= AppManager.Instance.LevelController.Game.ActiveLevel.CurrentMoney)
			_selectedTowerToBuild = tower;
	}

	public void DeselectTowerToBuild()
	{
		_selectedTowerToBuild = null;

		HideTowerUpgrade ();
	}

	public void ToggleFreeTowerArea()
	{
		_freeTowerAreaDisplayed = !_freeTowerAreaDisplayed;

		if (_freeTowerAreaDisplayed)
			ShowFreeTowerArea ();
		else
			HideFreeTowerArea ();
	}

	public void ShowFreeTowerArea()
	{
		List<TowerArea> towerAreaList = AppManager.Instance.LevelController.Game.activeTowerAreaList;

		for (int i = 0; i < towerAreaList.Count; ++i)
		{
			towerAreaList [i].ShowArea ();
		}

		_freeTowerAreaDisplayed = true;
		HideTowerUpgrade ();
	}

	public void HideFreeTowerArea()
	{
		DeselectTowerToBuild ();

		List<TowerArea> towerAreaList = AppManager.Instance.LevelController.Game.activeTowerAreaList;

		for (int i = 0; i < towerAreaList.Count; ++i)
		{
			towerAreaList [i].HideArea ();
		}

		_freeTowerAreaDisplayed = false;
		HideTowerUpgrade ();
	}

	public void ToggleBuiltTowerRange()
	{
		_builtTowerRangeDisplayed = !_builtTowerRangeDisplayed;

		if (_builtTowerRangeDisplayed)
			ShowBuiltTowerRange ();
		else
			HideBuiltTowerRange ();
	}

	public void ShowBuiltTowerRange()
	{
		HashSet<Tower> towerAreaList = AppManager.Instance.LevelController.Game.activeTowerSet;

		foreach (Tower t in towerAreaList)
		{
			t.ShowRange ();
		}
	}

	public void HideBuiltTowerRange()
	{
		HashSet<Tower> towerAreaList = AppManager.Instance.LevelController.Game.activeTowerSet;

		foreach (Tower t in towerAreaList)
		{
			t.HideRange ();
		}
	}

	public void ShowTowerUpgrade(TowerArea area)
	{
		if (area == null || area == _selectedAreaToUpgrade)
			return;
		
		HideTowerUpgrade ();
		
		_selectedAreaToUpgrade = area;

		GlobalEvent.Instance.DispatchCEvent(new CEvent(this, IDCE.Entity.onTowerUpgradeShow, new IDCEArgs.AreaDefault(area)));
	}

	public void HideTowerUpgrade()
	{
		if (_selectedAreaToUpgrade == null)
			return;

		_selectedAreaToUpgrade = null;
		GlobalEvent.Instance.DispatchCEvent(new CEvent(this, IDCE.Entity.onTowerUpgradeHide, null));
	}

	public void ClosePopups()
	{
		HideTowerUpgrade ();
	}
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
