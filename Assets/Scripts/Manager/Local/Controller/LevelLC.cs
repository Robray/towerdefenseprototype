﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	LevelLC class.
/// </summary>
public class LevelLC : LocalController 
{
	#region Public properties
	public GameCC Game
	{ get { return _game; } }

	public InteractionCC Interaction
	{ get { return _interaction; } }

	public UserInterfaceCC UI
	{ get { return _ui; } }

	public PoolCC Pool
	{ get { return _pool; } }
	#endregion

	#region Private properties
	private GameCC _game = new GameCC();
	private InteractionCC _interaction = new InteractionCC();
	private UserInterfaceCC _ui = new UserInterfaceCC();
	[SerializeField] PoolCC _pool = new PoolCC();
	#endregion

	#region API
	#endregion

	#region Unity
	protected override void Awake()
	{
		_controllerComponents.Add (_game);
		_controllerComponents.Add (_ui);
		_controllerComponents.Add (_interaction);
		_controllerComponents.Add (_pool);

		base.Awake ();
	}
	#endregion

	#region Private methods
	#endregion

	#region SubClass
	#endregion
}
