﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaCl.Events;

/// <summary>
///	UIConfig class.
/// </summary>
public class UIConfig : MBEventDispatcher 
{
	#region Public properties
	[Header("Panel")]
	public PressPanel startingPanel;
	public PressPanel winPanel;
	public PressPanel losePanel;
	[Space(5)]
	public TowerUpgradePanel towerUpgradePanel;

	[Header("Text")]
	public Text lifeText;
	public Text goldText;
	public Text waveText;
	public Text timeRemainingWaveText;

	[Header("Button")]
	public TowerBuildButton[] towerBuildButton;
	public Button sendWaveButton;
	public Button buyButton;

	[Header("Settings")]
	public float showSendWaveTime = 20;
	#endregion

	#region Private properties
	#endregion

	#region API
	#endregion

	#region Unity
	private void Awake() 
	{
		GlobalEvent.Instance.AddCEventListener(IDCE.Game.onLevelSceneLoaded, OnLevelSceneLoadedCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Game.onLevelWin, OnLevelWinCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Game.onLevelLose, OnLevelLoseCE);

		GlobalEvent.Instance.AddCEventListener(IDCE.Entity.onTowerUpgradeShow, OnTowerUpgradeShowCE);
		GlobalEvent.Instance.AddCEventListener(IDCE.Entity.onTowerUpgradeHide, OnTowerUpgradeHideCE);

		for (int i = 0; i < towerBuildButton.Length; ++i) 
		{
			if(towerBuildButton[i].button != null && towerBuildButton[i].tower != null)
			{
				Tower tower = towerBuildButton [i].tower;
				towerBuildButton [i].button.Initialize (tower);
			}
		}

		sendWaveButton.onClick.AddListener (() => {AppManager.Instance.LevelController.Interaction.SendNextWave(); });

		buyButton.onClick.AddListener (() => {
			AppManager.Instance.LevelController.Interaction.ToggleFreeTowerArea ();
			AppManager.Instance.LevelController.Interaction.ToggleBuiltTowerRange ();
		});
	}
	private void OnDestroy ()  
	{
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Game.onLevelSceneLoaded, OnLevelSceneLoadedCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Game.onLevelWin, OnLevelWinCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Game.onLevelLose, OnLevelLoseCE);

		GlobalEvent.Instance.RemoveCEventListener(IDCE.Entity.onTowerUpgradeShow, OnTowerUpgradeShowCE);
		GlobalEvent.Instance.RemoveCEventListener(IDCE.Entity.onTowerUpgradeHide, OnTowerUpgradeHideCE);
	}

	private void Update()
	{
		LevelConfig levelConfig = AppManager.Instance.LevelController.Game.ActiveLevel;

		if (levelConfig != null) 
		{
			waveText.text = levelConfig.CurrentWave + " / " + levelConfig.TotalWaves;

			goldText.text = "Gold : " + levelConfig.CurrentMoney;
			lifeText.text = "Life : " + levelConfig.CurrentLife;


			float timeRemainingWave = levelConfig.TimeRemainingBeforeWave;

			if(timeRemainingWave < 0)
				timeRemainingWaveText.text = "Send Wave";
			else
				timeRemainingWaveText.text = string.Format ("{0:0}", timeRemainingWave);

			if (timeRemainingWave >= showSendWaveTime || timeRemainingWave == 0)
				sendWaveButton.gameObject.SetActive (false);
			else
				sendWaveButton.gameObject.SetActive (true);
		}
	}
	#endregion

	#region Private methods
	//Events
	//	Main

	private void OnLevelSceneLoadedCE(CEvent e)
	{
		if (startingPanel == null)
		{
			AppManager.Instance.LevelController.Interaction.StartCurrentLevel ();
			return;
		}

		startingPanel.Show (AppManager.Instance.LevelController.Interaction.StartCurrentLevel);
	}
	private void OnLevelWinCE(CEvent e)
	{
		if (winPanel == null)
			return;

		winPanel.Show (AppManager.Instance.ReloadCurrentLevel);
	}
	private void OnLevelLoseCE(CEvent e)
	{
		if (losePanel == null)
			return;

		losePanel.Show (AppManager.Instance.ReloadCurrentLevel);
	}

	//	Tower

	private void OnTowerUpgradeShowCE(CEvent e)
	{
		if (towerUpgradePanel == null)
			return;

		IDCEArgs.AreaDefault areaArgs = e.args as IDCEArgs.AreaDefault;

		if (areaArgs == null)
			return;
		
		TowerArea area = areaArgs.area as TowerArea;

		towerUpgradePanel.Show (area);
	}
	private void OnTowerUpgradeHideCE(CEvent e)
	{
		if (towerUpgradePanel == null)
			return;

		towerUpgradePanel.Hide ();
	}
	#endregion

	#region SubClass
	[System.Serializable]
	public struct TowerBuildButton
	{
		public TowerBuyButton button;
		public Tower tower;
	}
	#endregion
}
