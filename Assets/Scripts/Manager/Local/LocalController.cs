﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.Events;

/// <summary>
///	LocalController class.
/// </summary>
public abstract class LocalController : MBEventDispatcher
{
	#region Public properties
	#endregion
	
	#region Private properties
	protected List<ControllerComponent> _controllerComponents = new List<ControllerComponent>();
	#endregion

	#region API
	#endregion
	
	#region Unity
	protected virtual void Awake()
	{
		for (int i = 0; i < _controllerComponents.Count; ++i)
			_controllerComponents [i].Initialize (this);
	}

	protected virtual void OnDestroy()
	{
		for (int i = 0; i < _controllerComponents.Count; ++i)
			_controllerComponents [i].Destroy (this);
	}
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}