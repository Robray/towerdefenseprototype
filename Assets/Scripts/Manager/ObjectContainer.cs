﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectContainer : MonoBehaviour
{
	[SerializeField][ReadOnlyRuntime] string _id = "";
	[SerializeField][ReadOnlyRuntime] bool _isPersitent = false;

	private bool _addedToDictionary = false;

	private static Dictionary<string, ObjectContainer> _containers = new Dictionary<string, ObjectContainer>();

	public static ObjectContainer GetContainer(string id)
	{
		ObjectContainer container;
		_containers.TryGetValue (id, out container);
		return container;
	}

	private void Awake()
	{
		ObjectContainer container;
		if (_containers.TryGetValue (_id, out container)) 
		{
			if (container == null) 
			{
				_containers.Remove (_id);

				_containers.Add (_id, this);
				_addedToDictionary = true;

				if(_isPersitent)
					DontDestroyOnLoad(gameObject);
			}
			else
			{
				Destroy(gameObject);
			}
		}
		else
		{
			_containers.Add(_id, this);
			_addedToDictionary = true;

			if(_isPersitent)
				DontDestroyOnLoad(gameObject);
		}
	}

	private void OnDestroy()
	{
		if (_addedToDictionary)
			_containers.Remove (_id);
	}
}

