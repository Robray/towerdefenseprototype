﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.Events;
using NaCl.ObjectPool;

/// <summary>
///	Entity class.
/// </summary>
public abstract class Entity : MBPoolableObj<Entity>  
{
	#region Public properties
	[ReadOnlyRuntime][SerializeField] string _id = "";

	public string ID
	{ get { return _id; } }

	public virtual Vector3 Position
	{
		get 
		{
			return transform.position;
		}
		set
		{
			transform.position = value;
		}
	}
	#endregion
	
	#region Private properties
	#endregion
	
	#region API
	#endregion
	
	#region Unity
	protected virtual void Awake()
	{
		Reset();
	}
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
