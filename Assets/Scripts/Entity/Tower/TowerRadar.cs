﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
///	TowerRadar class.
/// </summary>
public class TowerRadar : Tower 
{
	#region Public properties
	[Space(5)]
	[SerializeField] ParticleSystem _particles;
	#endregion
	
	#region Private properties
	private List<ICloaked> _revealedEnemyList = new List<ICloaked>();
	private List<ICloaked> _enemyInsideRangeList = new List<ICloaked>();
	#endregion
	
	#region API
	#endregion
	
	#region Unity
	//Override update to be able to target multiple targets.
	protected override void Update()
	{
		if (_linkedArea == null || _collector == null)
			return;

		_enemyInsideRangeList.Clear ();

		if (_collector.TargetList.Count > 0) 
		{
			for (int i = 0; i < _collector.TargetList.Count; ++i)
			{
				if (_collector.TargetList [i] is ICloaked)
					_enemyInsideRangeList.Add (_collector.TargetList [i] as ICloaked);
			}

			UnCloakTarget (_enemyInsideRangeList.Except (_revealedEnemyList).ToList ());
			CloakTarget (_revealedEnemyList.Except (_enemyInsideRangeList).ToList ());
		}
	}
	#endregion

	#region Private methods
	private void UnCloakTarget(List<ICloaked> target)
	{
		for (int i = 0; i < target.Count; ++i)
		{
			ICloaked character = target [i];

			if (character != null)
			{
				character.Uncloak ();
				_revealedEnemyList.Add (character);
			}
		}
	}
	private void CloakTarget(List<ICloaked> target)
	{
		for (int i = 0; i < target.Count; ++i)
		{
			ICloaked character = target [i];

			if (character != null)
			{
				character.Cloak ();
				_revealedEnemyList.Remove (character);
			}
		}
	}

	protected override void ActivateTower()
	{
		base.ActivateTower ();

		if(_particles != null)
			_particles.Play(true);
	}

	protected override void DeactivateTower()
	{
		if(_particles != null)
			_particles.Stop(true);
		
		base.DeactivateTower ();
	}
	#endregion
	
	#region SubClass
	#endregion
}
