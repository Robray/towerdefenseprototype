﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	TowerShieldBreak class.
/// </summary>
public class TowerShieldBreak : Tower 
{
	#region Public properties
	[Space(5)]
	public float damageShield = 10;
	public float damageLife = 1;
	[Space(5)]
	[SerializeField] ParticleSystem _particles;
	#endregion
	
	#region Private properties
	#endregion
	
	#region API
	#endregion
	
	#region Unity
	//Override update to be able to target multiple targets.
	protected override void Update()
	{
		if (_linkedArea == null || _collector == null)
			return;

		//If tower not allowed to shoot, wait
		if (_availableFireTime > Time.time)
			return;

		_availableFireTime = Time.time + rateOfFire;
		Shoot (_collector.TargetList);
	}
	#endregion

	#region Private methods
	protected override void Shoot(List<ITargetable> target)
	{
		if(_particles != null)
			_particles.Play(true);

		for (int i = 0; i < target.Count; ++i)
		{
			Character character = target [i] as Character;

			if (character != null && character.CanBeAttacked) 
			{
				float damage = damageLife;

				IArmored armoredCharacter = character as IArmored;

				//If character is armored, switch to armor damage.
				if(armoredCharacter != null && armoredCharacter.CurrentArmor > 0)
					damage = damageShield;

				character.GetDamage (damage);
			}
		}
	}
	#endregion
	
	#region SubClass
	#endregion
}
