﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.Events;

/// <summary>
///	Tower class.
/// </summary>
public abstract class Tower : Entity 
{
	#region Public properties
	public int cost = 0;
	public int recycleMoney = 0;
	[Space(5)]
	[SerializeField] GameObject _rangeObj;
	[SerializeField] Sprite _towerSprite;
	[SerializeField] protected TargetCollector _collector;
	[Space(5)]
	public float rateOfFire = 1;

	public Sprite TowerSprite
	{ get { return _towerSprite; } }

	public TowerArea LinkedArea
	{ get { return _linkedArea; } }
	#endregion
	
	#region Private properties
	protected TowerArea _linkedArea = null;
	protected float _availableFireTime = 0;
	#endregion
	
	#region API
	public override void Reset()
	{
		base.Reset ();

		_linkedArea = null;
		_availableFireTime = 0;
	}

	/// <summary>
	/// Display the range visualy.
	/// </summary>
	public void ShowRange()
	{
		if (_rangeObj == null)
			return;

		_rangeObj.SetActive (true);
	}

	/// <summary>
	/// Hide the range visualy.
	/// </summary>
	public void HideRange()
	{
		if (_rangeObj == null)
			return;

		_rangeObj.SetActive (false);
	}

	/// <summary>
	/// Link tower to an area. It activates the tower.
	/// </summary>
	public void LinkToArea(TowerArea area)
	{
		_linkedArea = area;

		if(!gameObject.activeInHierarchy)
			gameObject.SetActive (true);
		else
			ActivateTower ();

		transform.position = area.transform.position;
	}

	/// <summary>
	/// Remove tower from area. It deactivates the tower.
	/// </summary>
	public void UnlinkFromArea()
	{
		_linkedArea = null;

		gameObject.SetActive (false);
	}
	#endregion
	
	#region Unity
	private void OnEnable()
	{
		ActivateTower ();
	}

	private void OnDisable()
	{
		DeactivateTower ();
	}

	protected override void Awake()
	{
		base.Awake ();

		HideRange ();
	}

	protected virtual void Update()
	{
		if (_linkedArea == null || _collector == null)
			return;

		//If tower not allowed to shoot, wait
		if (_availableFireTime > Time.time)
			return;

		if (_collector.TargetList.Count > 0) 
		{
			_availableFireTime = Time.time + rateOfFire;
			Shoot (_collector.TargetList);
		}
	}
	#endregion

	#region Private methods
	protected virtual void Shoot(List<ITargetable> target)
	{
		Debug.Log ("Tower: Shoot");
	}

	protected virtual void ActivateTower()
	{
		if(_linkedArea != null)
			GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Entity.onTowerActivated, new IDCEArgs.EntityDefault (this)));
	}

	protected virtual void DeactivateTower()
	{
		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Entity.onTowerDeactivated, new IDCEArgs.EntityDefault (this)));
	}
	#endregion
	
	#region SubClass
	#endregion
}
