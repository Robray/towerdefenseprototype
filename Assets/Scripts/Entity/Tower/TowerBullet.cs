﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	TowerBullet class.
/// </summary>
public class TowerBullet : Tower 
{
	#region Public properties
	[Space(5)]
	public Projectile projectile;
	#endregion
	
	#region Private properties
	#endregion

	#region API
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	protected override void Shoot(List<ITargetable> target)
	{
		Character character = null;
		for(int i = 0; i < _collector.TargetList.Count; ++i)
		{
			character =	_collector.TargetList [i] as Character;
			if (character != null && character.CanBeAttacked)
				break;
		}

		if (character == null || projectile == null || !character.CanBeAttacked)
			return;

		Projectile p = AppManager.Instance.LevelController.Pool.InstantiateEntity<Projectile> (projectile.ID, transform.position, Quaternion.identity);

		if (p != null)
			p.SetTarget (character);
	}
	#endregion
	
	#region SubClass
	#endregion
}
