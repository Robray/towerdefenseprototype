﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	ICloaked class.
/// </summary>
public interface ICloaked
{
	bool IsCloaked{get;}

	void Cloak ();

	void Uncloak();
}
