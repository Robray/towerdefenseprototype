﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.Events;

/// <summary>
///	IEnemy class.
/// </summary>
public abstract class Enemy : Character, ITargetable
{
	#region Public properties
	public int bounty = 0;
	#endregion

	#region Private properties
	#endregion

	#region API
	public ITargetable GetTarget()
	{
		return this;
	}

	public override void Kill()
	{
		base.Kill ();
		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Entity.onEnemyKilled, new IDCEArgs.EntityDefault (this)));
		DispatchCEvent (new CEvent (this, IDCE.Entity.onEnemyKilled, null));
	}
	#endregion

	#region Unity
	private void OnEnable()
	{
		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Entity.onEnemyActivated, new IDCEArgs.EntityDefault (this)));
	}

	private void OnDisable()
	{
		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Entity.onEnemyDeactivated, new IDCEArgs.EntityDefault (this)));
		DispatchCEvent (new CEvent (this, IDCE.Entity.onEnemyDeactivated, null));
	}
	#endregion

	#region Private methods
	#endregion

	#region SubClass
	#endregion
}
