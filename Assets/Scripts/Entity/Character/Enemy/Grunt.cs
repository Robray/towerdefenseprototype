﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	Grunt class.
/// </summary>
public class Grunt : Enemy 
{
	#region Public properties
	#endregion
	
	#region Private properties
	#endregion
	
	#region API
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
