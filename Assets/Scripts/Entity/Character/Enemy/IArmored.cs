﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	IArmored class.
/// </summary>
public interface IArmored
{
	int CurrentArmor { get; }

	int MaxArmor { get ; }
}
