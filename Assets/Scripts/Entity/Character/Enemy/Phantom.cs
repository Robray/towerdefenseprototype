﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	Phantom class.
/// </summary>
public class Phantom : Enemy, ICloaked
{
	#region Public properties
	[SerializeField] SpriteRenderer _sprite;

	public bool IsCloaked
	{ get { return _isCloaked; } }

	public override bool CanBeAttacked
	{ get { return !_isCloaked; } }
	#endregion
	
	#region Private properties
	private float _alphaCloaked = 0.2f;
	private float _alphaUncloaked = 1f;

	private bool _isCloaked = false;
	#endregion
	
	#region API
	public override void Reset()
	{
		base.Reset ();

		Cloak ();
	}

	public void Cloak ()
	{
		SetAlpha (_alphaCloaked);
		_isCloaked = true;
	}

	public void Uncloak()
	{
		SetAlpha (_alphaUncloaked);
		_isCloaked = false;
	}
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	private void SetAlpha(float value)
	{
		if (_sprite != null) 
		{
			Color newColor = _sprite.color;
			newColor.a = value;

			_sprite.color = newColor;
		}
	}
	#endregion
	
	#region SubClass
	#endregion
}
