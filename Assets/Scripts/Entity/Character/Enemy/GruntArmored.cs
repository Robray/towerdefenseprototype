﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	GruntArmored class.
/// </summary>
public class GruntArmored : Grunt, IArmored
{
	#region Public properties
	public int startArmor = 1;

	public int CurrentArmor
	{ get { return _currentArmor; } }

	public int MaxArmor
	{ get { return startArmor; } }
	#endregion
	
	#region Private properties
	private int _currentArmor = 0;
	#endregion
	
	#region API
	public override void Reset()
	{
		base.Reset ();

		_currentArmor = startArmor;
	}

	public override void GetDamage(float damage)
	{
		//First damage the shield then the health
		if (_currentArmor > 0) 
		{
			_currentArmor -= (int) damage;

			if (_currentArmor < 0)
				_currentArmor = 0;
		} 
		else
			base.GetDamage (damage);
	}
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
