﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
///	Character class.
/// </summary>
[RequireComponent(typeof(NavMeshAgent))]
public abstract class Character : Entity 
{
	#region Public properties
	public float startLife = 10;

	public virtual bool CanBeAttacked
	{ get { return true; } }

	public float CurrentLife
	{ get { return _currentLife; } }

	public float MaxLife
	{ get { return startLife; } }

	public override Vector3 Position
	{
		get 
		{
			return transform.position;
		}
		set
		{
			_navAgent.Warp (value);
		}
	}
	#endregion
	
	#region Private properties
	protected float _currentLife = 0;

	private NavMeshAgent _navAgent = null;
	#endregion
	
	#region API

	public override void Reset()
	{
		_currentLife = startLife;

		if(_navAgent != null)
			_navAgent.ResetPath ();

		base.Reset ();
	}

	public virtual void GoTo(Vector3 destination)
	{
		_navAgent.SetDestination (destination);
	}

	public virtual void Attack(Character target)
	{
		
	}

	public virtual void GetDamage(float damage)
	{
		_currentLife -= damage;

		CheckState ();
	}

	public virtual void Kill()
	{
		Despawn ();
	}

	public virtual void Despawn()
	{
		Dispose ();
	}
	#endregion
	
	#region Unity
	protected override void Awake()
	{
		base.Awake ();

		_navAgent = GetComponent<NavMeshAgent> ();
	}
	#endregion

	#region Private methods
	protected void CheckState()
	{
		if (_currentLife <= 0) 
		{
			_currentLife = 0;
			Kill ();
		}
	}
	#endregion
	
	#region SubClass
	#endregion
}
