﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	Bullet class.
/// </summary>
public class Bullet : Projectile 
{
	#region Public properties
	public float damage = 1;
	public float speed = 1;
	[Range(0.001f, 10)] public float bulletRadius = 0.1f;
	#endregion
	
	#region Private properties
	#endregion
	
	#region API
	public override void SetTarget(Vector3 target)
	{
		base.SetTarget (target);

		StopAllCoroutines ();
		StartCoroutine ("GoToTargetProcess");
	}

	/*protected override void SetTarget(Character target)
	{
		base.SetTarget (target);
	}*/
	#endregion
	
	#region Unity
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.grey;
		Gizmos.DrawWireSphere (transform.position, bulletRadius);
	}
	#endregion

	#region Private methods
	private void OnTargetReached()
	{
		if (_targetChar != null) 
		{
			_targetChar.GetDamage (damage);
		}

		Dispose ();
	}

	private IEnumerator GoToTargetProcess()
	{
		float radius2 = bulletRadius * bulletRadius;

		while (SqrtDistanceFromTarget > radius2) 
		{
			yield return 0;
			transform.position = Vector3.MoveTowards (transform.position, TargetPosition, speed * Time.deltaTime);
		}

		OnTargetReached ();
	}
	#endregion
	
	#region SubClass
	#endregion
}
