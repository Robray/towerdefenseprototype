﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.Events;

/// <summary>
///	Projectile class.
/// </summary>
public class Projectile : Entity
{
	#region Public properties
	public float SqrtDistanceFromTarget
	{ get { return (transform.position - TargetPosition).sqrMagnitude; } }

	public Vector3 TargetPosition
	{ get { return _targetChar != null ? _targetChar.transform.position : _targetPos; } }
	#endregion
	
	#region Private properties
	protected Vector3 _targetPos;
	protected Character _targetChar;
	#endregion
	
	#region API
	public override void Reset()
	{
		base.Reset ();

		if (_targetChar != null)
		{
			_targetChar.RemoveCEventListener (IDCE.Entity.onEnemyKilled, OnTargetKilledCE);
			_targetChar = null;
		}
	}

	public virtual void SetTarget(Vector3 target)
	{
		_targetPos = target;
	}

	public virtual void SetTarget(Character target)
	{
		if (target == null)
			return;

		_targetChar = target;
		_targetChar.AddCEventListener (IDCE.Entity.onEnemyKilled, OnTargetKilledCE, true);

		SetTarget (_targetChar.transform.position);
	}
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	private void OnTargetKilledCE(CEvent e)
	{
		_targetChar = null;
	}
	#endregion
	
	#region SubClass
	#endregion
}
