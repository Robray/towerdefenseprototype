﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.Events;

/// <summary>
///	LifeArea class.
/// </summary>
[RequireComponent(typeof(TargetCollector), typeof(SphereCollider))]
public class LifeArea : Area 
{
	#region Public properties
	#endregion
	
	#region Private properties
	private SphereCollider _collider;
	private TargetCollector _collector;

	private float ColliderRadius
	{
		get
		{ 
			if (_collider == null)
				_collider = GetComponent<SphereCollider> ();
			return _collider.radius;
		}
	}
	#endregion
	
	#region API
	public Vector3 GetRandomPointInsideArea()
	{
		Vector2 randomPoint = Random.insideUnitCircle;

		Vector3 position = transform.rotation * (new Vector3(randomPoint.x, 0, randomPoint.y) * _collider.radius) + transform.position;

		return position;
	}
	#endregion
	
	#region Unity
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		GizmosUtils.DrawCicle (transform.position, transform.rotation, 25, ColliderRadius);
	}

	private void OnEnable()
	{
		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Environment.onLifeAreaActivated, new IDCEArgs.AreaDefault (this)));
	}

	private void OnDisable()
	{
		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Environment.onLifeAreaDeactivated, new IDCEArgs.AreaDefault (this)));
	}

	private void Awake()
	{
		_collider = GetComponent<SphereCollider> ();
		_collector = GetComponent<TargetCollector> ();
	}

	private void Update()
	{
		for (int i = 0; i < _collector.TargetList.Count; ++i) 
		{
			Enemy enemy = _collector.TargetList[i] as Enemy;

			if (enemy != null && AppManager.Instance.LevelController.Game.activeEnemySet.Contains(enemy)) 
			{
				GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Entity.onEnemyReachGoal, new IDCEArgs.EntityDefault (enemy)));
				enemy.Despawn ();
			}

		}
	}
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
