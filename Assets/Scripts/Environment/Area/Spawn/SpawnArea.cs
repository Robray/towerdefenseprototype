﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.Events;

/// <summary>
///	SpawnArea class.
/// </summary>
public class SpawnArea : Area 
{
	#region Public properties
	public float radius;
	#endregion
	
	#region Private properties
	#endregion
	
	#region API
	/*public static SpawnArea GetRandomArea()
	{
		if (_existingSpawnArea.Count == 0)
			return null;

		return _existingSpawnArea[Random.Range(0, _existingSpawnArea.Count)];
	}*/

	public Entity SpawnEntity(Entity entity)
	{
		if (entity == null)
			return null;

		Vector2 randomPoint = Random.insideUnitCircle;

		Vector3 position = transform.rotation * (new Vector3(randomPoint.x, 0, randomPoint.y) * radius) + transform.position;

		return AppManager.Instance.LevelController.Pool.InstantiateEntity<Entity> (entity.ID, position, Quaternion.identity);
	}
	#endregion
	
	#region Unity
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		GizmosUtils.DrawCicle (transform.position, transform.rotation, 25, radius);
	}

	private void OnEnable()
	{
		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Environment.onSpawnAreaActivated, new IDCEArgs.AreaDefault (this)));
	}

	private void OnDisable()
	{
		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Environment.onSpawnAreaDeactivated, new IDCEArgs.AreaDefault (this)));
	}
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
