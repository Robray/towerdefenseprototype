﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using NaCl.Events;

/// <summary>
///	TowerArea class.
/// </summary>
public class TowerArea : Area
{
	#region Public properties
	[SerializeField] EventTrigger _areaEventObj;
	[SerializeField] EventTrigger _towerEventObj;

	public bool IsFree
	{ get { return _towerBuilt == null; } }

	public Tower TowerBuilt
	{ get { return _towerBuilt; } }
	#endregion
	
	#region Private properties
	private Tower _towerBuilt = null;
	private Tower _towerPreview = null;
	#endregion
	
	#region API
	/// <summary>
	/// Display area icon.
	/// </summary>
	public void ShowArea()
	{
		_areaEventObj.gameObject.SetActive (true);
	}

	/// <summary>
	/// Hide area icon.
	/// </summary>
	public void HideArea()
	{
		_areaEventObj.gameObject.SetActive (false);
	}

	/// <summary>
	/// Display tower preview on area.
	/// </summary>
	public void ShowPreviewTower()
	{
		Tower tower = AppManager.Instance.LevelController.Interaction.SelectedTowerToBuild;

		if (tower == null)
			return;

		_towerPreview = AppManager.Instance.LevelController.Pool.InstantiateEntity<Tower> (tower.ID, transform.position, Quaternion.identity);

		if(_towerPreview != null)
			_towerPreview.ShowRange ();
	}

	/// <summary>
	/// Hide tower preview on area.
	/// </summary>
	public void HidePreviewTower()
	{
		if (_towerPreview == null)
			return;
		
		_towerPreview.HideRange ();

		_towerPreview.Dispose ();
		_towerPreview = null;
	}

	//Used by event trigger on child object
	public void ShowTowerUpgrade()
	{
		AppManager.Instance.LevelController.Interaction.ShowTowerUpgrade (this);
	}

	/// <summary>
	/// Build tower and remove area from active list.
	/// </summary>
	public void BuildTower()
	{
		AppManager.Instance.LevelController.Interaction.ClosePopups (); //TODO: temp

		if (_towerPreview == null)
			return;

		_towerBuilt = _towerPreview;
		_towerPreview = null;

		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Entity.onTowerBought, new IDCEArgs.EntityDefault (_towerBuilt)));
		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Environment.onTowerBuildAreaDeactivated, new IDCEArgs.AreaDefault (this)));

		_towerBuilt.LinkToArea (this);

		_towerEventObj.gameObject.SetActive (true);
		HideArea ();
	}

	/// <summary>
	/// Destroy tower and add area to active list.
	/// </summary>
	public void DestroyTower()
	{
		if (_towerBuilt == null)
			return;

		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Entity.onTowerDestroyed, new IDCEArgs.EntityDefault (_towerBuilt)));

		_towerBuilt.UnlinkFromArea ();

		_towerBuilt.Dispose();
		_towerBuilt = null;

		_towerEventObj.gameObject.SetActive (false);
		AppManager.Instance.LevelController.Interaction.HideTowerUpgrade ();

		if (AppManager.Instance.LevelController.Interaction.FreeTowerAreaDisplayed) //TODO: redo the whole show area event call
			ShowArea ();

		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Environment.onTowerBuildAreaActivated, new IDCEArgs.AreaDefault (this)));
	}
	#endregion
	
	#region Unity
	private void Awake()
	{
		HideArea ();
		_towerEventObj.gameObject.SetActive (false);

		GlobalEvent.Instance.DispatchCEvent (new CEvent (this, IDCE.Environment.onTowerBuildAreaActivated, new IDCEArgs.AreaDefault (this)));
	}
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
