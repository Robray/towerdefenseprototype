﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaCl.Events;

/// <summary>
///	IArea class.
/// </summary>
public class Area : MBEventDispatcher 
{
	#region Public properties
	#endregion
	
	#region Private properties
	#endregion
	
	#region API
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
