﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
///	PressPanel class.
/// </summary>
[RequireComponent(typeof(CanvasGroup))]
public class PressPanel : MonoBehaviour 
{
	#region Public properties
	public float showTime = 1;
	public float hideTime = 1;
	#endregion
	
	#region Private properties
	private CanvasGroup _canvasGroup = null;
	private Action _onHide = null;
	#endregion
	
	#region API
	public void Show()
	{Show (null);}
	public void Show(Action onHide)
	{
		StopAllCoroutines ();

		gameObject.SetActive (true);
		_onHide = onHide;

		Action<float> loop = (t) => {
			_canvasGroup.alpha = EasingUtils.easeInQuad (0, 1, t);
		};
		Action end = () => {
			_canvasGroup.alpha = 1;
			StartCoroutine("WaitForPressProcess");
		};

		StartCoroutine(this.CoroutineProgressUnscaled(showTime, loop, end));
	}

	public void Hide()
	{
		StopAllCoroutines ();

		Action<float> loop = (t) => {
			_canvasGroup.alpha = EasingUtils.easeInQuad (1, 0, t);
		};
		Action end = () => {
			_canvasGroup.alpha = 0;
			if(_onHide != null)
			{
				_onHide();
				_onHide = null;
			}

			gameObject.SetActive (false);
		};

		StartCoroutine(this.CoroutineProgressUnscaled(hideTime, loop, end));
	}
	#endregion
	
	#region Unity
	private void Awake()
	{
		_canvasGroup = GetComponent<CanvasGroup> ();
	}
	#endregion

	#region Private methods
	private IEnumerator WaitForPressProcess()
	{
		while(InputManager.GetTouchCount() <= 0)
		{
			yield return 0;
		}

		Hide();
	}
	#endregion
	
	#region SubClass
	#endregion
}
