﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
///	TowerUpgradePanel class.
/// </summary>
[RequireComponent(typeof(RectTransform))]
public class TowerUpgradePanel : MonoBehaviour 
{
	#region Public properties
	[SerializeField] Button _destroyTowerButton;
	#endregion
	
	#region Private properties
	private TowerArea _linkedArea = null;
	private RectTransform _rectTransform = null;
	#endregion
	
	#region API
	public void Show(TowerArea area)
	{
		if (area == null)
			return;

		_linkedArea = area;

		gameObject.SetActive (true);

		Vector2 viewPos = Camera.main.WorldToViewportPoint (area.transform.position);

		_rectTransform.anchorMin = viewPos;
		_rectTransform.anchorMax = viewPos;
	}

	public void Hide()
	{
		_linkedArea = null;

		gameObject.SetActive (false);
	}

	public void DestroyTower()
	{
		if (_linkedArea == null)
			return;
		
		_linkedArea.DestroyTower ();
	}
	#endregion
	
	#region Unity
	private void Awake()
	{
		_rectTransform = GetComponent<RectTransform> ();

		if (_destroyTowerButton != null) 
		{
			_destroyTowerButton.onClick.AddListener (DestroyTower);
		}
	}
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
