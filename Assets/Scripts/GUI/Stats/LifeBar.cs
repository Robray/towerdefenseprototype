﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaCl.ObjectPool;
using NaCl.Events;

/// <summary>
///	LifeBar class.
/// </summary>
[RequireComponent(typeof(RectTransform))]
public class LifeBar : MBPoolableObj<LifeBar>
{
	#region Public properties
	[SerializeField] Image _lifeBar;
	[SerializeField] Image _shieldBar;
	[Space(5)]
	public Vector2 offset = Vector2.zero;
	#endregion
	
	#region Private properties
	private RectTransform _rectTransform = null;

	private Character _target = null;
	private IArmored _targetArmored = null;
	#endregion
	
	#region API
	public override void Reset()
	{
		base.Reset ();

		if (_target != null) 
		{
			_target.RemoveCEventListener (IDCE.Entity.onEnemyDeactivated, OnTargetDeactivateddCE);

			_target = null;
			_targetArmored = null;
		}
	}

	public void LinkToCharacter(Character target)
	{
		if (target == null)
			return;

		_target = target;
		_targetArmored = _target as IArmored;

		SetPosition (_target);

		if(_targetArmored == null || _targetArmored.MaxArmor <= 0)
			SetBarRatio (_shieldBar.rectTransform, 0);
		if(_target.MaxLife <= 0)
			SetBarRatio (_lifeBar.rectTransform, 0);

		_target.AddCEventListener(IDCE.Entity.onEnemyDeactivated, OnTargetDeactivateddCE, true);
	}
	#endregion
	
	#region Unity
	private void Awake()
	{
		_rectTransform = GetComponent<RectTransform> ();
	}

	private void OnDisable()
	{
		if (_target != null)
			_target.RemoveCEventListener (IDCE.Entity.onEnemyDeactivated, OnTargetDeactivateddCE);
	}

	private void Update()
	{
		if (_target == null)
			return;

		SetPosition (_target);

		if (_targetArmored != null && _targetArmored.MaxArmor > 0) 
		{
			float shieldRatio = (float)_targetArmored.CurrentArmor / _targetArmored.MaxArmor;

			SetBarRatio (_shieldBar.rectTransform, shieldRatio);
		}

		if (_target.MaxLife > 0)
		{
			float lifeRatio = _target.CurrentLife / _target.MaxLife;

			SetBarRatio (_lifeBar.rectTransform, lifeRatio);
		}
	}
	#endregion

	#region Private methods
	private void SetPosition(Character target)
	{
		Vector2 viewPos = Camera.main.WorldToViewportPoint (target.transform.position);
		viewPos += offset;

		_rectTransform.anchorMin = viewPos;
		_rectTransform.anchorMax = viewPos;
	}

	private void SetBarRatio(RectTransform rt, float ratio)
	{
		Vector3 newScale = rt.localScale;
		newScale.x = ratio;

		rt.localScale = newScale;
	}

	private void OnTargetDeactivateddCE(CEvent e)
	{
		Invoke ("Dispose", 0.01f); //TODO: temp, invoke necessary, event dispatching bugs without
	}
	#endregion
	
	#region SubClass
	#endregion
}
