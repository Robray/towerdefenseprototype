﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
///	TowerBuyButton class.
/// </summary>
public class TowerBuyButton : MonoBehaviour 
{
	#region Public properties
	[SerializeField] Text _costText;
	[SerializeField] Image _sprite;
	[SerializeField] Image _background;
	[SerializeField] Button _buyButton;
	#endregion
	
	#region Private properties
	#endregion
	
	#region API
	public void Initialize(Tower tower)
	{
		if (tower == null)
			return;

		if (_sprite != null) 
		{
			_sprite.sprite = tower.TowerSprite;
		}
	
		if (_costText != null) 
		{
			_costText.text = string.Format("{0}", tower.cost);
		}

		_buyButton.onClick.AddListener(() => { AppManager.Instance.LevelController.Interaction.SelectTowerToBuild (tower);});
	}
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
