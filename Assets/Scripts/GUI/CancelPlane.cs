﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	CancelPlane class.
/// </summary>
public class CancelPlane : MonoBehaviour 
{
	#region Public properties
	#endregion
	
	#region Private properties
	#endregion
	
	#region API
	public void CancelAction()
	{
		AppManager.Instance.LevelController.Interaction.ClosePopups ();
	}
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
