﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
///	UIMover class.
/// </summary>
public class UIMover : MonoBehaviour 
{
	#region Public properties
	public CanvasGroup target;
	public GameObject pointIn;
	public GameObject pointOut;
	[Space(5)]
	public float timeIn = 0;
	public EasingUtils.EaseType easeIn = EasingUtils.EaseType.linear;
	public float timeOut = 0;
	public EasingUtils.EaseType easeOut = EasingUtils.EaseType.linear;
	[Space(5)]
	public bool startIn = true;
	public bool interactableIn = true;
	public bool interactableOut = true;
	#endregion
	
	#region Private properties
	private Coroutine _moveCR = null;
	private bool _isIn = false;
	#endregion
	
	#region API
	public void MoveIn()
	{
		if (pointIn == null || pointOut == null)
			return;

		MoveFromTo (pointOut.transform.position, pointIn.transform.position, timeIn, easeIn, interactableIn);
	}

	public void MoveOut()
	{
		if (pointIn == null || pointOut == null)
			return;

		MoveFromTo (pointIn.transform.position, pointOut.transform.position, timeOut, easeOut, interactableOut);
	}

	public void MoveToggle()
	{
		_isIn = !_isIn;

		if (_isIn)
			MoveIn ();
		else
			MoveOut();
	}
	#endregion
	
	#region Unity
	private void Awake()
	{
		if (pointIn == null || pointOut == null)
			return;
		
		if (startIn)
			MoveFromTo (pointIn.transform.position, pointIn.transform.position, 0, easeIn, interactableIn);
		else
			MoveFromTo (pointOut.transform.position, pointOut.transform.position, 0, easeOut, interactableOut);

		_isIn = startIn;
	}
	#endregion

	#region Private methods
	private void MoveFromTo(Vector3 from, Vector3 to, float time, EasingUtils.EaseType ease, bool allowInteractionOnEnd = false)
	{
		if (target == null)
			return;

		Action end = () => {
			target.transform.position = to;

			target.interactable = allowInteractionOnEnd;
		};

		if (time <= 0) 
		{
			end ();
			return;
		}

		Action<float> loop = (t) => {
			Vector3 newPos;

			newPos.x = EasingUtils.GetEaseFunction(ease)(from.x, to.x, t);
			newPos.y = EasingUtils.GetEaseFunction(ease)(from.y, to.y, t);
			newPos.z = EasingUtils.GetEaseFunction(ease)(from.z, to.z, t);

			target.transform.position = newPos;
		};
			
		target.interactable = false;

		this.StopCoroutineSafe (ref _moveCR);
		_moveCR = StartCoroutine (this.CoroutineProgress (time, loop, end));
	}
	#endregion
	
	#region SubClass
	#endregion
}
