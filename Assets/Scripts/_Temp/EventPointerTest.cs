﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	EventPointerTest class.
/// </summary>
public class EventPointerTest : MonoBehaviour 
{
	#region Public properties
	#endregion
	
	#region Private properties
	#endregion
	
	#region API
	public void PointerEnter()
	{
		Debug.Log ("EventPointerTest: POINTER ENTER");
	}

	public void PointerExit()
	{
		Debug.Log ("EventPointerTest: POINTER EXIT");
	}

	public void PointerClick()
	{
		Debug.Log ("EventPointerTest: POINTER CLICK");
	}

	public void PointerUp()
	{
		Debug.Log ("EventPointerTest: POINTER UP");
	}
	#endregion
	
	#region Unity
	#endregion

	#region Private methods
	#endregion
	
	#region SubClass
	#endregion
}
